# RasterEngine

A 3D software rasterisation renderer.

# Compile

Building the project requires a C++14 compiler and SFML (http://www.sfml-dev.org/).

The makefile is for compiling with MinGW64 on windows. It will create a statically
linked executable with no dependencies. For other systems it is possible to use the
CodeBlocks project file.

To use your own build system link with the system, window and graphics parts of 
SFML.
