
// Source code layout style.

// The naming conventions are:
//      Variables, constants and enums: camelCase, example thisVariable.
//      Routines and classes: CamelCase with first letter capitalized, example MyFunction().
//      Macros: All upper-case with underscores, example THIS_IS_A_MACRO (try not to use macros).
//      Namespaces: Lower-case with first letter capitalized, example Space.

// A tab is 4 white-spaces.
// Limit lines to 100 white-spaces.

// Declare everything on separate lines. Do not use 'int x, y;' use:
int x; 
int y;

// Pointers and references are connected to the type.
int* a;
int& FunctionGo(int& a, int* b, int* c, int d);


// If statement layout.
if (someCondition) {
    // Code.
}

if (someCondition) {
    // Code.
}
else {
    // Code.
}

if (someCondition) {
    // Code.
}
else if (someCondition) {
    // Code.
}
else {
    // Code.
}


// Switch statement layout.
switch (value) {
    case someCase:
        // Code.
        break;
    case anotherCase:
        // Code.
        break;
    default:
        // Code.
        break;
}


// Loop layouts.
while (someCondition) {
    // Code.
}

do {
    // Code.
} while (someCondition);

for (int i = 0; i < 10; i++) {
    // Code.
}


// Line continuation layouts.
int bigLongVariableName = 1;
int bigBigLongVariableName = 2;
int bigBigBigLongVariableName = 3;

int sumTotalVariableName = bigLongVariableName + bigBigLongVariableName +
    bigBigBigLongVariableName;

    
// We pass parameters to a Function by:
int someX = FunctionGo( bigLongVariableName, bigBigLongVariableName,
    bigBigBigLongVariableName, sumTotalVariableName );
// ... or by:
int someX = FunctionGo(
    bigLongVariableName,
    bigBigLongVariableName,
    bigBigBigLongVariableName,
    sumTotalVariableName
);


// This is just an illustration. Consider rewriting the below code with boolean variables or with
// a function MyBooleanTest() to simplify it.
if ( bigLongVariableName < bigBigLongVariableName &&
    bigBigBigLongVariableName <= sumTotalVariableName
) {

    // Code.

    if ( bigLongVariableName < bigBigLongVariableName &&
        bigBigBigLongVariableName <= sumTotalVariableName
    ) {
        // Code.
    }
    else {
        // Code.
    }

}


// Function declaration layout.
// ... small parameter list.
int FunctionGo(int a, int b, int c, int d) {
    return a*b*c*d;
}

// ... large parameter list.
int FunctionGoGo(
    int bigLongVariableName,
    int bigBigLongVariableName,
    int bigBigBigLongVariableName,
    int sumTotalVariableName
) {
    return bigLongVariableName*bigBigLongVariableName;
}


// Class declaration layout.
// Comment, describing what Vector does.
class Vector
{
public:

    Vector();
    ~Vector();

    // Public methods.

    // Public members, constants and enumerations.

protected:

    // Protected methods.

    // Protected members, constants and enumerations.

private:

    // Private methods.

    // Private members, constants and enumerations.

};


// Structure of header files.
/*
    1: Short comment describing the file contents.
    2: Include guard.
    3: Include files.
    4: Forward declarations.
    7: Typedefs.
    5: Constants.
    6: Variables.
    8: Functions.
    9: Classes.
*/


// Structure of implementation files.
/*
    1: Short comment describing the file.
    2: Include files.
    3: Variables.
    4: Classes/Functions.

    Each section should be separated by a major comment as shown below.
*/


//
// Example of an implementation file.
// Vector, FunctionGo and FunctionGoGo is declared in Vector.hpp. This is
// Vector.cpp.
//

////////////////////////////////////////////////////////////////////////
//
// Vector.cpp
//
// License, copyright, author yadayada..
//
////////////////////////////////////////////////////////////////////////


#include "Vector.hpp"


////////////////////////////////////////////////////////////////////////
// Vector class
//
// Short description.
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Vector::Vector()
{
    // Code.
}


////////////////////////////////////////////////////////////////////////
Vector::~Vector()
{
    // Code.
}


////////////////////////////////////////////////////////////////////////
int Vector::SomeMethod(
    int sourceVariableName, // Comment if needed.
    int targetVariableName  // Comment if needed.
)
{
    // Code.
}


////////////////////////////////////////////////////////////////////////
// GoGo functions
//
// Short description.
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
int FunctionGo(int a, int b, int c, int d)
{
    return a*b*c*d;
}


////////////////////////////////////////////////////////////////////////
int FunctionGoGo(
    int bigLongVariableName, // Comment if needed.
    int bigBigLongVariableName, // Comment if needed.
    int bigBigBigLongVariableName, // Comment if needed.
    int sumTotalVariableName // Comment if needed.
)
{
    return bigLongVariableName*bigBigLongVariableName;
}
