# Makefile. Made for Windows PowerShell/Command Prompt.

# Specify directories. Use '.' for no directory.
EXE_DIR = build
OBJ_DIR = obj/release-makefile
SRC_DIR = src
SRC_SUB_DIRS = Core FileFormat

EXE_NAME = Engine.exe
SRC_EXT = cpp

INCLUDE_DIRS = -IC:/Programming/Libraries/SFML-2.4.0/include
LIBRARY_DIRS = -LC:/Programming/Libraries/SFML-2.4.0/build-mingw64/lib

# Static linking requires: https://github.com/LaurentGomila/SFML/wiki/FAQ#build-link-static
LINK_LIBRARIES = \
	-lsfml-graphics-s \
	-lsfml-window-s \
	-lsfml-system-s \
	-lfreetype \
	-ljpeg \
	-lwinmm \
	-lopengl32 \
	-lgdi32

# Warning flags.Too many false-positives:
# -Winline -Wconversion -Wdouble-promotion -Wswitch-default -Weffc++
WARNING_FLAGS_CPLUSPLUS = \
	-Wctor-dtor-privacy -Wdelete-non-virtual-dtor -Wliteral-suffix -Wnarrowing \
	-Wnoexcept -Wnon-virtual-dtor -Wreorder -Wstrict-null-sentinel \
	-Wold-style-cast -Wbool-compare -Woverloaded-virtual -Wsign-promo \
	-Wsuggest-final-types -Wsuggest-final-methods -Wzero-as-null-pointer-constant \
	-Wuseless-cast -Wconditionally-supported
WARNING_FLAGS = \
	-Wpedantic -Wall -Wextra -Wformat=2 -Wmissing-include-dirs -Wunused \
	-Wuninitialized -Wunknown-pragmas -Wfloat-equal -Wundef -Wshadow \
	-Wpointer-arith -Wcast-qual -Wcast-align -Wlogical-not-parentheses \
	-Wswitch-bool -Wlogical-op -Wbool-compare -Wmissing-declarations \
	-Wnormalized=nfkc -Wopenmp-simd -Wpacked -Wredundant-decls -Winvalid-pch \
	-Wvector-operation-performance -Wdisabled-optimization -Wstack-protector \
	-Wunused-macros -Wsizeof-array-argument -Wbool-compare \
	-Wshift-negative-value -Wnull-dereference -Wduplicated-cond \
	$(WARNING_FLAGS_CPLUSPLUS)

PREPROCESSOR = -DSFML_STATIC
COMPILE_FLAGS = $(INCLUDE_DIRS) $(PREPROCESSOR) -O3 -std=c++14 $(WARNING_FLAGS)
LINKING_FLAGS = $(LIBRARY_DIRS) $(LINK_LIBRARIES) -s -static -mwindows $(WARNING_FLAGS)

### No Editing Should Be Needed Below This Line ###

# Find all source files in the source and its sub directories.
SOURCES = $(wildcard $(SRC_DIR)/*.$(SRC_EXT)) $(wildcard $(SRC_DIR)/*/*.$(SRC_EXT))
# Set full object file names.
OBJECTS = $(addprefix $(OBJ_DIR)/, $(SOURCES:.$(SRC_EXT)=.o))
# Set the dependency files that will be used to add header dependencies
DEPENDS = $(OBJECTS:.o=.d)
# Set full executable file name.
EXECUTABLE = $(addprefix $(EXE_DIR)/, $(EXE_NAME))
# Set object file directories.
SRC_DIRS = $(SRC_DIR) $(addprefix $(SRC_DIR)/, $(SRC_SUB_DIRS))
OBJ_DIRS = $(OBJ_DIR) $(addprefix $(OBJ_DIR)/, $(SRC_DIRS))

# Build executable.
.PHONY: all
all: $(EXECUTABLE)

# Link object files together.
$(EXECUTABLE): $(OBJECTS) | $(EXE_DIR)
	@echo "Linking: Object files -> $(notdir $(EXECUTABLE))"
	@g++ $(OBJECTS) $(LINKING_FLAGS) -o $(EXECUTABLE)

# Build object files.
$(OBJECTS): $(OBJ_DIR)/%.o: %.$(SRC_EXT) | $(OBJ_DIRS)
	@echo "Compiling: $(notdir $<) -> $(notdir $@)"
	@g++ $(COMPILE_FLAGS) -MMD -MP -c $< -o $@

# Add dependency targets. Copies the target text in dependency files.
-include $(DEPENDS)

# Create object and executable directories.
$(EXE_DIR) $(OBJ_DIRS):
	mkdir $(subst /,\,$@)

# Remove object files, dependency files and executable.
.PHONY: clean
clean:
	@del $(subst /,\,$(EXECUTABLE))
	@del $(subst /,\,$(OBJECTS))
	@del $(subst /,\,$(DEPENDS))
	@echo "Cleaning Done."
