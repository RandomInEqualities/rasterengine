////////////////////////////////////////////////////////////////////////
//
// Scene.hpp - Window that displays a 3D scene. Controls window display,
//             user input and rendering.
//
////////////////////////////////////////////////////////////////////////

#ifndef SCENE_HPP_INCLUDED
#define SCENE_HPP_INCLUDED

#include <SFML/Graphics.hpp>

#include <map>
#include <vector>
#include "Entity.hpp"
#include "Model.hpp"
#include "Camera.hpp"
#include "Renderer.hpp"
#include "Time.hpp"
#include "Core/Types.hpp"
#include "Core/Vector2.hpp"


// Scene handles a camera, entities (models, objects), rendering of entities, displaying the
// rendered images and handling user input events.
class Scene
{
public:

    Scene();

    // Begin the scene event loop. Returns when user exits the scene window.
    void Begin();

private:

    // Create (or recreate) the window.
    void CreateWindow();

    // Set the mouse position to the middle of the window.
    void CentreMouseInWindow();

    // Set the window and renderer resolution. Recreates the window if it is open.
    void SetResolution(Vector2<unsigned int> resolution);

    // Set the horizontal or vertical field of view.It sets both the horizontal and vertical fov.
    // The one you do not specify is computed to match the physical monitor.
    void SetFOV(Real degrees, bool horizontal = true);

    //
    // Event Processing
    //

    // Process window events. Polls all events, distributes relevant events and updates mKeyIsPressed.
    void ProcessEvents();

    // Modifies the scene according to key and mouse events.
    void HandleLostFocus();
    void HandleKeyPress(const sf::Event& event);
    void HandleKeyRelease(const sf::Event& event);
    void HandleMouseMove(const sf::Event& event);

    // Update the camera and entity logic.
    void UpdateCameraPosition(Seconds elapsedTime);
    void UpdateEntities(Seconds elapsedTime);

    //
    // Scene parameters
    //

    // The view camera.
    Camera mCamera;

    // All entities in the scene.
    std::vector<Entity> mSceneEntities;
    TextureContainer mSceneTextures;

    // Renderer used to generate 2D images of the scene.
    Renderer mRenderer;

    // Output window
    sf::RenderWindow mWindow;

    Vector2<unsigned int> mResolution;
    Vector2<Real> mFov;
    sf::Uint32 mWindowStyle;
    bool mWindowVsync;

    // Frame start times.
    TimePoint mFrameStartTime;
    TimePoint mLastFrameStartTime;

    // Keyboard keys that are currently pressed. Is kept updated by the ProcessEvents function.
    std::map<sf::Keyboard::Key, bool> mKeyIsPressed;

    // The rendering and display of an image of the scene works by:
    // 1) The camera view transform, window resolution and zoom is passed to the renderer.
    // 2) The renderer receives a frame buffer which it renders the image into.
    // 3) We convert the frame buffer to a graphics card texture, which the window displays.

    // Frame buffer that holds the rendered 2D images.
    std::vector<Uint8> mFrameBuffer;

    // Texture and sprite that the window uses to display an image with. The texture lives on the
    // graphics card.
    sf::Texture mWindowTexture;
    sf::Sprite mWindowSprite;

};


#endif // SCENE_HPP_INCLUDED
