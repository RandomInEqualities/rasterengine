////////////////////////////////////////////////////////////////////////
//
// Camera.hpp - Camera class to represent a camera in a 3D scene.
//
////////////////////////////////////////////////////////////////////////

#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include "Time.hpp"
#include "Core/Types.hpp"
#include "Core/Vector3.hpp"
class Matrix4x4;


class Camera
{
public:

    Camera();
    Camera(const Vector3<Real>& position, const Vector3<Real>& target, const Vector3<Real>& up);

    // Set the camera position, look at target and up direction.
    void Init(const Vector3<Real>& position, const Vector3<Real>& target, const Vector3<Real>& up);
    void SetPosition(const Vector3<Real>& position);
    void SetTarget(const Vector3<Real>& target);
    void SetUp(const Vector3<Real>& up);

    Vector3<Real> GetPosition() const;
    Vector3<Real> GetTarget() const;
    Vector3<Real> GetUp() const;

    // Get the matrix that transform from world space to camera space.
    Matrix4x4 GetViewTransform() const;

    // Modify the camera position and orientation. Can emulate a smoothly moving camera.
    void MoveForward(Seconds elapsedTime);
    void MoveBackwards(Seconds elapsedTime);
    void StrafeLeft(Seconds elapsedTime);
    void StrafeRight(Seconds elapsedTime);
    void TiltRight(Seconds elapsedTime);
    void TiltLeft(Seconds elapsedTime);

    // Modify the camera position and orientation. Can emulate mouse movement.
    void MouseMoved(Real deltaX, Real deltaY);

    // The move speed in meters/second.
    Real moveSpeed{5.0};

    // The tilt speed in radians/second.
    Real tiltSpeed{4.0};

private:

    // Tilt camera. Negative seconds tilts to the right. Positive to the left.
    void Tilt(Seconds elapsedTime);

    // Calculate the camera basis (camAxisX,Y,Z) from the position, target and up direction.
    void CalculateCameraBasis();

    Vector3<Real> mPosition;
    Vector3<Real> mTarget;
    Vector3<Real> mUp;

    // The camera basis vectors.
    Vector3<Real> camAxisX;
    Vector3<Real> camAxisY;
    Vector3<Real> camAxisZ;

    Real mouseSensitivityX{0.5};
    Real mouseSensitivityY{0.5};

};


#endif // CAMERA_HPP_INCLUDED
