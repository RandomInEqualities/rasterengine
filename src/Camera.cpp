
#include "Camera.hpp"
#include "Time.hpp"

#include "Core/Vector3.hpp"
#include "Core/Matrix3x3.hpp"
#include "Core/Matrix4x4.hpp"
#include "Core/Math.hpp"

#include "Core/Types.hpp"
#include "Core/Debug.hpp"


////////////////////////////////////////////////////////////////////////
Camera::Camera()
{
    Init(Vector3<Real>{0.0,0.0,0.0}, Vector3<Real>{1.0,0.0,0.0}, Vector3<Real>{0.0,1.0,0.0});
}


////////////////////////////////////////////////////////////////////////
Camera::Camera(const Vector3<Real>& position, const Vector3<Real>& target, const Vector3<Real>& up)
{
    Init(position, target, up);
}


////////////////////////////////////////////////////////////////////////
void Camera::Init(const Vector3<Real>& position, const Vector3<Real>& target,
                  const Vector3<Real>& up)
{
    ASSERT(!Math::IsZero(up.Length()), "");
    ASSERT(!Math::IsZero((position-target).Length()), "");

    mPosition = position;
    mTarget = target;
    mUp = up;
    mUp.Normalize();
    CalculateCameraBasis();
}


////////////////////////////////////////////////////////////////////////
void Camera::SetPosition(const Vector3<Real>& position)
{
    Init(position, mTarget, mUp);
}


////////////////////////////////////////////////////////////////////////
void Camera::SetTarget(const Vector3<Real>& target)
{
    Init(mPosition, target, mUp);
}


////////////////////////////////////////////////////////////////////////
void Camera::SetUp(const Vector3<Real>& up)
{
    Init(mPosition, mTarget, up);
}


////////////////////////////////////////////////////////////////////////
Vector3<Real> Camera::GetPosition() const
{
    return mPosition;
}


////////////////////////////////////////////////////////////////////////
Vector3<Real> Camera::GetTarget() const
{
    return mPosition + camAxisZ;
}


////////////////////////////////////////////////////////////////////////
Vector3<Real> Camera::GetUp() const
{
    return camAxisY;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4 Camera::GetViewTransform() const
{
    // Translate from world to camera.
    Matrix4x4 translation;
    translation.SetupTranslation(-mPosition);

    // Rotate into the camera basis.
    Matrix3x3 rotation;
    rotation.m00 = camAxisX.x; rotation.m01 = camAxisY.x; rotation.m02 = camAxisZ.x;
    rotation.m10 = camAxisX.y; rotation.m11 = camAxisY.y; rotation.m12 = camAxisZ.y;
    rotation.m20 = camAxisX.z; rotation.m21 = camAxisY.z; rotation.m22 = camAxisZ.z;

    return Multiply(rotation.Inverse(), translation);
}


////////////////////////////////////////////////////////////////////////
void Camera::MoveForward(Seconds elapsedTime)
{
    mPosition -= elapsedTime.count()*moveSpeed*camAxisZ;
}


////////////////////////////////////////////////////////////////////////
void Camera::MoveBackwards(Seconds elapsedTime)
{
    mPosition += elapsedTime.count()*moveSpeed*camAxisZ;
}


////////////////////////////////////////////////////////////////////////
void Camera::StrafeLeft(Seconds elapsedTime)
{
    mPosition -= elapsedTime.count()*moveSpeed*camAxisX;
}


////////////////////////////////////////////////////////////////////////
void Camera::StrafeRight(Seconds elapsedTime)
{
    mPosition += elapsedTime.count()*moveSpeed*camAxisX;
}


////////////////////////////////////////////////////////////////////////
void Camera::TiltRight(Seconds elapsedTime)
{
    Tilt(-elapsedTime);
}


////////////////////////////////////////////////////////////////////////
void Camera::TiltLeft(Seconds elapsedTime)
{
    Tilt(elapsedTime);
}


////////////////////////////////////////////////////////////////////////
void Camera::MouseMoved(Real deltaX, Real deltaY)
{
    // Horizontal mouse movement
    if (!Math::IsZero(deltaX)) {
        Vector3<Real> rotationAxis = -camAxisY;
        Real rotationAngle = mouseSensitivityX*2*Math::pi*deltaX;

        Matrix3x3 rotation;
        rotation.SetupRotation(rotationAxis, rotationAngle);

        camAxisX *= rotation;
        camAxisZ *= rotation;
    }

    // Vertical mouse movement
    if (!Math::IsZero(deltaY)) {
        Vector3<Real> rotationAxis = -camAxisX;
        Real rotationAngle = mouseSensitivityY*2.0*Math::pi*deltaY;

        Matrix3x3 rotation;
        rotation.SetupRotation(rotationAxis, rotationAngle);

        camAxisY *= rotation;
        camAxisZ *= rotation;
    }
}


////////////////////////////////////////////////////////////////////////
void Camera::Tilt(Seconds elapsedTime)
{
    Real angle = tiltSpeed*elapsedTime.count();
    Matrix3x3 rotation;
    rotation.SetupRotation(camAxisZ, angle);
    camAxisX *= rotation;
    camAxisY *= rotation;
}


////////////////////////////////////////////////////////////////////////
void Camera::CalculateCameraBasis()
{
    camAxisZ = mPosition-mTarget;
    camAxisZ.Normalize();

    camAxisX = CrossProduct(mUp, camAxisZ);
    camAxisX.Normalize();

    camAxisY = CrossProduct(camAxisZ, camAxisX);
    camAxisY.Normalize();
}
