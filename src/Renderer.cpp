////////////////////////////////////////////////////////////////////////
//
// Renderer.cpp
//
////////////////////////////////////////////////////////////////////////


#include <vector>
#include <array>
#include <utility>
#include <cstring>

#include "Renderer.hpp"
#include "Entity.hpp"
#include "Model.hpp"

#include "Core/Types.hpp"
#include "Core/Debug.hpp"
#include "Core/Vector2.hpp"
#include "Core/Vector3.hpp"
#include "Core/Vector4.hpp"
#include "Core/Matrix4x4.hpp"
#include "Core/Math.hpp"


////////////////////////////////////////////////////////////////////////
//
// RendererTriangle
//
////////////////////////////////////////////////////////////////////////


// Triangle that goes through the rendering pipeline. The triangles that models have (the Triangle
// class) is not optimized and can not have extra parameters that are nice to carry around.
// We store pointers directly to the triangle colour and texture for maximum performance.
class RendererTriangle
{
public:

    // Triangle coordinates in clip space.
    Vector4<Real> p0c;
    Vector4<Real> p1c;
    Vector4<Real> p2c;

    // Parameters that are used to calculate the barycentric coordinates of a point on
    // the triangle. See Barycentric Coordinates as Interpolants for more info.
    Real w1w2alpha0;
    Real w2w0alpha1;
    Real w0w1alpha2;
    Real w1w2beta0;
    Real w2w0beta1;
    Real w0w1beta2;
    Real w1w2gamma0;
    Real w2w0gamma1;
    Real w0w1gamma2;

    // What should be drawn.
    bool drawColours;
    bool drawWireframe;

    // The triangle colour and texture.
    const Uint8* uniformColour;
    Vector2<Real> p0TexturePos;
    Vector2<Real> p1TexturePos;
    Vector2<Real> p2TexturePos;
    int textureId;
    int textureWidth;
    int textureHeight;
    const Uint8* textureData;

    // Initialize the colour parameters from a triangle or another structure.
    void Initialize(const Triangle& triangle, const TextureContainer* textures);
    void Initialize(const RendererTriangle& triangle, bool updateTexturePos);

};


void RendererTriangle::Initialize(const Triangle& triangle, const TextureContainer* textures)
{
    uniformColour = triangle.uniformColour.rgba.data();
    textureId = triangle.textureId;
    if (triangle.textureId != -1) {
        const Texture& texture = textures->GetTexture(triangle.textureId);
        textureWidth = texture.width;
        textureHeight = texture.height;
        textureData = texture.rgba.data();
        p0TexturePos.x = texture.width*triangle.p0TexturePos.x;
        p0TexturePos.y = texture.height*triangle.p0TexturePos.y;
        p1TexturePos.x = texture.width*triangle.p1TexturePos.x;
        p1TexturePos.y = texture.height*triangle.p1TexturePos.y;
        p2TexturePos.x = texture.width*triangle.p2TexturePos.x;
        p2TexturePos.y = texture.height*triangle.p2TexturePos.y;
    }
}


void RendererTriangle::Initialize(const RendererTriangle& triangle, bool updateTexturePos)
{
    drawColours = triangle.drawColours;
    drawWireframe = triangle.drawWireframe;
    uniformColour = triangle.uniformColour;
    textureId = triangle.textureId;
    if (triangle.textureId != -1) {
        textureWidth = triangle.textureWidth;
        textureHeight = triangle.textureHeight;
        textureData = triangle.textureData;
        if (updateTexturePos) {
            p0TexturePos = triangle.p0TexturePos;
            p1TexturePos = triangle.p1TexturePos;
            p2TexturePos = triangle.p2TexturePos;
        }
    }
}


////////////////////////////////////////////////////////////////////////
//
// Renderer public methods
//
////////////////////////////////////////////////////////////////////////


Renderer::Renderer() :
mFrameBuffer(nullptr),
mResolution(0,0),
mResolutionOver2(0,0),
mZoom(1.0,1.0),
mTextures(nullptr),
mDrawBackFacingTriangles(false),
mDrawColours(true),
mDrawWireframes(false),
mWireframeColour(255,255,255,255),
mDepthData(nullptr),
mNearZ(0.05),
mFarZ(200.0)
{
    ComputeCameraToClipMatrix();
}


void Renderer::Render(std::vector<Uint8>& output, const std::vector<Entity>& entities)
{
    mFrameBuffer = output.data();

    // Make sure output image is large enough for the specified resolution.
    size_t imageSize = mResolution.x*mResolution.y;
    ASSERT(4*imageSize <= output.size(), "");

    // Resize depth buffer to fit the image.
    if (mDepthBuffer.size() != imageSize) {
        mDepthBuffer.resize(imageSize);
        mDepthData = mDepthBuffer.data();
    }

    ClearBuffers();

    // Transform and draw the triangles in the entity models.
    Matrix4x4 worldToClip = mCameraToClip*mWorldToCamera;

    for (size_t index = 0; index < entities.size(); index++) {
        const Entity& entity = entities[index];

        Matrix4x4 entityToWorld;
        entityToWorld.SetupLocalToParent(entity.position, entity.rotation);
        Matrix4x4 entityToClip = worldToClip*entityToWorld;

        for (size_t i = 0; i < entity.model.geometry.size(); i++) {
            const Triangle& triangle = entity.model.geometry[i];

            // Project each vertex into clip space.
            RendererTriangle rendererTriangle;
            rendererTriangle.p0c = entityToClip*triangle.p0;
            rendererTriangle.p1c = entityToClip*triangle.p1;
            rendererTriangle.p2c = entityToClip*triangle.p2;

            // Specify how to draw it.
            rendererTriangle.drawColours = mDrawColours;
            rendererTriangle.drawWireframe = mDrawWireframes;
            rendererTriangle.Initialize(triangle, mTextures);

            RenderTriangle(rendererTriangle);
        }
    }

    mFrameBuffer = nullptr;
}


void Renderer::SetViewTransform(const Matrix4x4& worldToCamera)
{
    mWorldToCamera = worldToCamera;
}


void Renderer::SetTextures(const TextureContainer* textures)
{
    mTextures = textures;
}


void Renderer::SetResolution(Vector2<unsigned int> resolution)
{
    mResolution.x = static_cast<int>(resolution.x);
    mResolution.y = static_cast<int>(resolution.y);
    mResolutionOver2.x = static_cast<int>(resolution.x/2);
    mResolutionOver2.y = static_cast<int>(resolution.y/2);
}


void Renderer::SetZoom(Vector2<Real> zoom)
{
    ASSERT(!Math::IsZero(zoom.x) && !Math::IsZero(zoom.y), "");
    mZoom = zoom;
    ComputeCameraToClipMatrix();
}


void Renderer::DrawColours(bool enabled)
{
    mDrawColours = enabled;
}


void Renderer::DrawWireframes(bool enabled)
{
    mDrawWireframes = enabled;
}


bool Renderer::IsDrawingColours() const
{
    return mDrawColours;
}


bool Renderer::IsDrawingWireframes() const
{
    return mDrawWireframes;
}


////////////////////////////////////////////////////////////////////////
//
// Renderer private methods
//
////////////////////////////////////////////////////////////////////////


void Renderer::RenderTriangle(RendererTriangle& tri)
{
    if (!tri.drawColours && !tri.drawWireframe) {
        return;
    }

    // Don't draw colours on back-facing triangles.
    if (!mDrawBackFacingTriangles && tri.drawColours) {

        // Compute the clip space triangle normal.
        Vector3<Real> p0{tri.p0c.x, tri.p0c.y, tri.p0c.w};
        Vector3<Real> p0p1{tri.p1c.x - tri.p0c.x, tri.p1c.y - tri.p0c.y, tri.p1c.w - tri.p0c.w};
        Vector3<Real> p0p2{tri.p2c.x - tri.p0c.x, tri.p2c.y - tri.p0c.y, tri.p2c.w - tri.p0c.w};
        Vector3<Real> normal = CrossProduct(p0p1, p0p2);

        if (DotProduct(p0, normal) < 0) {
            tri.drawColours = false;
        }
    }

    bool drawTriangle = ClipWithFrustrum(tri);
    if (!drawTriangle) {
        return;
    }

    DrawTriangle(tri);
}


void Renderer::ComputeCameraToClipMatrix()
{
    // Construct the clip matrix. The clip matrix does three things:
    // - Put the projection divisor into w. Usually perspective projection is division by z, now
    //   you should divide by w (w is equal to z if the clip matrix does perspective projection).
    // - Scale x and y by the horizontal and vertical zoom. x -> x*mZoom.x, y -> y*mZoom.y.
    // - Normalize the z coordinate, such that the near clip plane is at z = -w, and far clip
    //   plane at z = w.
    mCameraToClip.m01 = mCameraToClip.m02 = mCameraToClip.m10 = mCameraToClip.m12 = 0.0;
    mCameraToClip.m20 = mCameraToClip.m21 = mCameraToClip.m30 = mCameraToClip.m31 = 0.0;
    mCameraToClip.tx = mCameraToClip.ty = mCameraToClip.tw = 0.0;

    mCameraToClip.m00 = mZoom.x;
    mCameraToClip.m11 = mZoom.y;
    mCameraToClip.m22 = (mFarZ + mNearZ)/(mNearZ - mFarZ);
    mCameraToClip.tz = 2*mFarZ*mNearZ/(mNearZ - mFarZ);
    mCameraToClip.m32 = -1.0;

    /*
    // Change to these values for orthographic projection.
    mCameraToClip.m22 = 2.0/(mNearZ - mFarZ);
    mCameraToClip.tz = (mFarZ + mNearZ)/(mNearZ - mFarZ);
    mCameraToClip.tw = 1.0;
    mCameraToClip.m32 = 0.0;
    */
}


void Renderer::ClearBuffers()
{
    // Clear the depth and frame buffer.
    const int size = mResolution.x*mResolution.y;

    for (int index = 0; index < size; index++) {
        mDepthData[index] = realMax;
    }

    std::memset(mFrameBuffer, 100, 4*size*sizeof(Uint8));
}


bool Renderer::ClipWithFrustrum(const RendererTriangle& tri)
{
    // An arbitrary point p is inside the view frustum if:
    //      -p.w < p.x < p.w
    //      -p.w < p.y < p.w
    //      -p.w < p.z < p.w
    // This is true because of how x and y are scaled to the screen and because the clip matrix
    // scales z. We use this to determine what triangles to draw, clip and discard.
    bool p0cOutNear = tri.p0c.z < -tri.p0c.w;
    bool p0cOutFar = tri.p0c.w < tri.p0c.z;
    bool p0cOutLeft = tri.p0c.x < -tri.p0c.w;
    bool p0cOutRight = tri.p0c.w < tri.p0c.x;
    bool p0cOutBot = tri.p0c.y < -tri.p0c.w;
    bool p0cOutTop = tri.p0c.w < tri.p0c.y;

    bool p1cOutNear = tri.p1c.z < -tri.p1c.w;
    bool p1cOutFar = tri.p1c.w < tri.p1c.z;
    bool p1cOutLeft = tri.p1c.x < -tri.p1c.w;
    bool p1cOutRight = tri.p1c.w < tri.p1c.x;
    bool p1cOutBot = tri.p1c.y < -tri.p1c.w;
    bool p1cOutTop = tri.p1c.w < tri.p1c.y;

    bool p2cOutNear = tri.p2c.z < -tri.p2c.w;
    bool p2cOutFar = tri.p2c.w < tri.p2c.z;
    bool p2cOutLeft = tri.p2c.x < -tri.p2c.w;
    bool p2cOutRight = tri.p2c.w < tri.p2c.x;
    bool p2cOutBot = tri.p2c.y < -tri.p2c.w;
    bool p2cOutTop = tri.p2c.w < tri.p2c.y;

    // Any triangle that are completely outside the left, right, bot, top, near or far frustum
    // planes are discarded.
    if (p0cOutNear && p1cOutNear && p2cOutNear) {
        return false;
    }
    if (p0cOutFar && p1cOutFar && p2cOutFar) {
        return false;
    }
    if (p0cOutLeft && p1cOutLeft && p2cOutLeft) {
        return false;
    }
    if (p0cOutRight && p1cOutRight && p2cOutRight) {
        return false;
    }
    if (p0cOutBot && p1cOutBot && p2cOutBot) {
        return false;
    }
    if (p0cOutTop && p1cOutTop && p2cOutTop) {
        return false;
    }

    // Clip triangles that are partly inside the near clip plane.
    if (p0cOutNear || p1cOutNear || p2cOutNear) {
        ClipWithPlane(tri, ClipPlane::near, p0cOutNear, p1cOutNear, p2cOutNear);
        return false;
    }

    // We could clip triangles that are inside the far, left, right, bottom and top clip planes,
    // but we do not gain much from it and rounding errors can result in lines between triangles.
    // I should find a fix for this.

    return true;
}


void Renderer::ClipWithPlane(const RendererTriangle& tri, Renderer::ClipPlane plane,
    bool p0cOutside, bool p1cOutside, bool p2cOutside)
{
    Vector4<Real> p0 = tri.p0c;
    Vector4<Real> p1 = tri.p1c;
    Vector4<Real> p2 = tri.p2c;
    Vector2<Real> p0TexturePos = tri.p0TexturePos;
    Vector2<Real> p1TexturePos = tri.p1TexturePos;
    Vector2<Real> p2TexturePos = tri.p2TexturePos;

    // When we clip a triangle with a plane one vertices is on one side of the plane and two
    // vertices are on the other side. We order p0, p1 and p2 such that p0 is the 'lonely' one.
    bool invertedNormal = false;
    if ((p0cOutside && p2cOutside) || (!p0cOutside && !p2cOutside)) {
        std::swap(p0, p1);
        std::swap(p0TexturePos, p1TexturePos);
        std::swap(p0cOutside, p1cOutside);
        invertedNormal = true;
    }
    else if ((p0cOutside && p1cOutside) || (!p0cOutside && !p1cOutside)) {
        std::swap(p0, p2);
        std::swap(p0TexturePos, p2TexturePos);
        std::swap(p0cOutside, p2cOutside);
        invertedNormal = true;
    }

    // Find the intersections between the clip plane and p0p1 and p0p2. Utilises that we have either
    // x = w, x = -w, y = w, y = -w, z = w or z = -w at the clip planes. For example the near clip
    // plane has z = -w. This makes it possible to interpolate the intersections.
    Real t1 = 0;
    Real t2 = 0;
    switch (plane)
    {
        case ClipPlane::near:
            t1 = (p0.z + p0.w)/(p0.z + p0.w - p1.z - p1.w);
            t2 = (p0.z + p0.w)/(p0.z + p0.w - p2.z - p2.w);
            break;
        case ClipPlane::far:
            t1 = (p0.z - p0.w)/(p0.z - p0.w - p1.z + p1.w);
            t2 = (p0.z - p0.w)/(p0.z - p0.w - p2.z + p2.w);
            break;
        case ClipPlane::left:
            t1 = (p0.x + p0.w)/(p0.x + p0.w - p1.x - p1.w);
            t2 = (p0.x + p0.w)/(p0.x + p0.w - p2.x - p2.w);
            break;
        case ClipPlane::right:
            t1 = (p0.x - p0.w)/(p0.x - p0.w - p1.x + p1.w);
            t2 = (p0.x - p0.w)/(p0.x - p0.w - p2.x + p2.w);
            break;
        case ClipPlane::bottom:
            t1 = (p0.y + p0.w)/(p0.y + p0.w - p1.y - p1.w);
            t2 = (p0.y + p0.w)/(p0.y + p0.w - p2.y - p2.w);
            break;
        case ClipPlane::top:
            t1 = (p0.y - p0.w)/(p0.y - p0.w - p1.y + p1.w);
            t2 = (p0.y - p0.w)/(p0.y - p0.w - p2.y + p2.w);
            break;
    }
    Vector4<Real> p0p1intersection = p0 + (p1 - p0)*t1;
    Vector4<Real> p0p2intersection = p0 + (p2 - p0)*t2;

    // Due to rounding errors it can happen that the intersections are outside the view frustum.
    // If this happens we get infinite recursion.
    switch (plane)
    {
        case ClipPlane::near:
            p0p1intersection.z = -p0p1intersection.w;
            p0p2intersection.z = -p0p2intersection.w;
            break;
        case ClipPlane::far:
            p0p1intersection.z = p0p1intersection.w;
            p0p2intersection.z = p0p2intersection.w;
            break;
        case ClipPlane::left:
            p0p1intersection.x = -p0p1intersection.w;
            p0p2intersection.x = -p0p2intersection.w;
            break;
        case ClipPlane::right:
            p0p1intersection.x = p0p1intersection.w;
            p0p2intersection.x = p0p2intersection.w;
            break;
        case ClipPlane::bottom:
            p0p1intersection.y = -p0p1intersection.w;
            p0p2intersection.y = -p0p2intersection.w;
            break;
        case ClipPlane::top:
            p0p1intersection.y = p0p1intersection.w;
            p0p2intersection.y = p0p2intersection.w;
            break;
    }

    if (!p0cOutside) {

        // The lonely point is within the view frustum and we need one new triangle. Construct it
        // from p0 and the intersections.
        RendererTriangle newTriangle;
        newTriangle.p0c = p0;
        newTriangle.p1c = p0p1intersection;
        newTriangle.p2c = p0p2intersection;
        newTriangle.p0TexturePos = p0TexturePos;
        newTriangle.p1TexturePos = p0TexturePos + (p1TexturePos - p0TexturePos)*t1;
        newTriangle.p2TexturePos = p0TexturePos + (p2TexturePos - p0TexturePos)*t2;
        if (invertedNormal) {
            std::swap(newTriangle.p0c, newTriangle.p1c);
            std::swap(newTriangle.p0TexturePos, newTriangle.p1TexturePos);
        }
        newTriangle.Initialize(tri, false);
        RenderTriangle(newTriangle);

    }
    else {

        // The lonely point is outside the view frustum and we need two new triangles. Construct
        // them from p1, p2 and the intersections.
        RendererTriangle newTriangle1;
        newTriangle1.p0c = p0p1intersection;
        newTriangle1.p1c = p1;
        newTriangle1.p2c = p2;
        newTriangle1.p0TexturePos = p0TexturePos + (p1TexturePos - p0TexturePos)*t1;
        newTriangle1.p1TexturePos = p1TexturePos;
        newTriangle1.p2TexturePos = p2TexturePos;
        if (invertedNormal) {
            std::swap(newTriangle1.p0c, newTriangle1.p1c);
            std::swap(newTriangle1.p0TexturePos, newTriangle1.p1TexturePos);
        }
        newTriangle1.Initialize(tri, false);
        RenderTriangle(newTriangle1);

        RendererTriangle newTriangle2;
        newTriangle2.p0c = p0p2intersection;
        newTriangle2.p1c = p0p1intersection;
        newTriangle2.p2c = p2;
        newTriangle2.p0TexturePos = p0TexturePos + (p2TexturePos - p0TexturePos)*t2;
        newTriangle2.p1TexturePos = p0TexturePos + (p1TexturePos - p0TexturePos)*t1;
        newTriangle2.p2TexturePos = p2TexturePos;
        if (invertedNormal) {
            std::swap(newTriangle2.p0c, newTriangle2.p1c);
            std::swap(newTriangle2.p0TexturePos, newTriangle2.p1TexturePos);
        }
        newTriangle2.Initialize(tri, false);
        RenderTriangle(newTriangle2);

    }
}


void Renderer::DrawTriangle(RendererTriangle& tri)
{
    // In this function we partition the triangle into horizontal lines and draw them from left
    // to right. The actual drawing is done by the ProcessScanLine function, this function tells
    // which lines that ProcessScanLine should draw.

    // The triangle coordinates in screen space.
    Vector2<int> p0s;
    Vector2<int> p1s;
    Vector2<int> p2s;
    p0s.x = static_cast<int>((1.0 + tri.p0c.x/tri.p0c.w)*mResolutionOver2.x);
    p0s.y = static_cast<int>((1.0 - tri.p0c.y/tri.p0c.w)*mResolutionOver2.y);
    p1s.x = static_cast<int>((1.0 + tri.p1c.x/tri.p1c.w)*mResolutionOver2.x);
    p1s.y = static_cast<int>((1.0 - tri.p1c.y/tri.p1c.w)*mResolutionOver2.y);
    p2s.x = static_cast<int>((1.0 + tri.p2c.x/tri.p2c.w)*mResolutionOver2.x);
    p2s.y = static_cast<int>((1.0 - tri.p2c.y/tri.p2c.w)*mResolutionOver2.y);

    // Draw the triangle outline and exit if that is all we needed to draw.
    if (tri.drawWireframe) {
        DrawLine(p0s, p1s, mWireframeColour);
        DrawLine(p0s, p2s, mWireframeColour);
        DrawLine(p1s, p2s, mWireframeColour);
    }
    if (!tri.drawColours) {
        return;
    }

    // Calculate the barycentric parameters that are constant across the triangle. We pass
    // these to the ProcessScanLine functions such that they don't need to recalculate them.
    // See Barycentric Coordinates as Interpolants for a complete explanation.
    Real w0w1 = tri.p0c.w*tri.p1c.w;
    Real w2w0 = tri.p0c.w*tri.p2c.w;
    Real w1w2 = tri.p1c.w*tri.p2c.w;
    tri.w1w2alpha0 = w1w2*(p1s.y - p2s.y);
    tri.w2w0alpha1 = w2w0*(p2s.y - p0s.y);
    tri.w0w1alpha2 = w0w1*(p0s.y - p1s.y);
    tri.w1w2beta0 = w1w2*(p2s.x - p1s.x);
    tri.w2w0beta1 = w2w0*(p0s.x - p2s.x);
    tri.w0w1beta2 = w0w1*(p1s.x - p0s.x);
    tri.w1w2gamma0 = w1w2*(p1s.x*p2s.y - p2s.x*p1s.y);
    tri.w2w0gamma1 = w2w0*(p2s.x*p0s.y - p0s.x*p2s.y);
    tri.w0w1gamma2 = w0w1*(p0s.x*p1s.y - p1s.x*p0s.y);

    // Order the points such that p0s.y < p1s.y < p2s.y. The barycentric parameters must be
    // calculated before we do this.
    if (p2s.y < p1s.y) {
        std::swap(p2s, p1s);
    }
    if (p2s.y < p0s.y) {
        std::swap(p2s, p0s);
    }
    if (p1s.y < p0s.y) {
        std::swap(p1s, p0s);
    }

    // Only draw the line part inside the screen.
    int yMin = p0s.y;
    int yMax = p2s.y;
    if (yMin < 0) {
        yMin = 0;
    }
    if (mResolution.y < yMax) {
        yMax = mResolution.y;
    }

    // If the line is outside the screen we are done.
    if (yMax < 0 || mResolution.y <= yMin || yMax <= yMin) {
        return;
    }

    // Draw each horizontal line in the triangle, left to right, for each y value. With the way
    // that the triangle is ordered we draw from p0p2 to p0p1 and p1p2 or we draw from p0p1 and
    // p1p2 to p0p2 (make a drawing:).
    if ((p1s.x-p0s.x)*(p2s.y-p0s.y) < (p2s.x-p0s.x)*(p1s.y-p0s.y)) {
        for (int y = yMin; y < yMax; y++) {
            if (y < p1s.y) {
                ProcessScanLine(y, p0s, p1s, p0s, p2s, tri);
            }
            else {
                ProcessScanLine(y, p1s, p2s, p0s, p2s, tri);
            }
        }
    }
    else {
        for (int y = yMin; y < yMax; y++) {
            if (y < p1s.y) {
                ProcessScanLine(y, p0s, p2s, p0s, p1s, tri);
            }
            else {
                ProcessScanLine(y, p0s, p2s, p1s, p2s, tri);
            }
        }
    }
}


void Renderer::ProcessScanLine(int y, const Vector2<int>& a, const Vector2<int>& b,
    const Vector2<int>& c, const Vector2<int>& d, const RendererTriangle& tri)
{
    // At the given y value, this function draws a horizontal line from ab to cd.

    // Find the minimum and maximum x value to draw between.
    Real gradientAB = (a.y == b.y) ? 1.0 : static_cast<Real>(y - a.y)/static_cast<Real>(b.y - a.y);
    Real gradientCD = (c.y == d.y) ? 1.0 : static_cast<Real>(y - c.y)/static_cast<Real>(d.y - c.y);
    int xMin = static_cast<int>(a.x + (b.x - a.x)*gradientAB);
    int xMax = static_cast<int>(c.x + (d.x - c.x)*gradientCD);

    // Only draw the part inside the screen.
    if (xMin < 0) {
        xMin = 0;
    }
    if (mResolution.x < xMax) {
        xMax = mResolution.x;
    }

    // If the line is outside the screen we are done.
    if (xMax < xMin || mResolution.x <= xMin || xMax < 0) {
        return;
    }

    // As in Barycentric Coordinates as Interpolants we calculate the initial areas of the triangle.
    // A0 is the area spanned by back-projecting (x,y), p1s and p2s back to eye space and then
    // finding the triangle area, the formula has been reduced to yield very few computational
    // steps. A1 and A2 are respectively the area spanned by the back-projection of (x,y), p0s, p2s
    // and (x,y), p0s, p1s.
    Real A0 = tri.w1w2alpha0*xMin + tri.w1w2beta0*y + tri.w1w2gamma0;
    Real A1 = tri.w2w0alpha1*xMin + tri.w2w0beta1*y + tri.w2w0gamma1;
    Real A2 = tri.w0w1alpha2*xMin + tri.w0w1beta2*y + tri.w0w1gamma2;

    for (int x = xMin; x < xMax; x++) {

        // Find the barycentric coordinates of this point. Uses the calculated eye space triangle
        // areas. We can use these coordinates to interpolate any vertex level property.
        Real total = A0 + A1 + A2;
        Real b0 = A0/total;
        Real b1 = A1/total;
        Real b2 = 1.0 - b0 - b1;

        // Find the depth of this point, used to determine visibility.
        Real w = b0*tri.p0c.w + b1*tri.p1c.w + b2*tri.p2c.w;

        // Draw the point into the frame buffer.
        DrawPoint(x, y, w, b0, b1, b2, tri);

        // Update the areas to be the areas at x+1. Updating like this is faster than
        // recalculating for each x.
        A0 += tri.w1w2alpha0;
        A1 += tri.w2w0alpha1;
        A2 += tri.w0w1alpha2;
    }
}


void Renderer::DrawPoint(int x, int y, Real w, Real b0, Real b1, Real b2,
    const RendererTriangle& tri)
{
    // Check if the point is visible.
    const int depthIndex = x + y*mResolution.x;
    if (mDepthData[depthIndex] < w) {
        return;
    }
    mDepthData[depthIndex] = w;

    // Determine the colour of the point and put it into the frame buffer.
    const int frameIndex = 4*depthIndex;
    if (tri.textureId != -1) {
        int col = b0*tri.p0TexturePos.x + b1*tri.p1TexturePos.x + b2*tri.p2TexturePos.x;
        int row = b0*tri.p0TexturePos.y + b1*tri.p1TexturePos.y + b2*tri.p2TexturePos.y;
        col = Math::Clamp(col, 0, tri.textureWidth - 1);
        row = Math::Clamp(row, 0, tri.textureHeight - 1);
        PutPixel(frameIndex, tri.textureData + 4*(col + row*tri.textureWidth));
    }
    else {
        PutPixel(frameIndex, tri.uniformColour);
    }
}


void Renderer::DrawLine(const Vector2<int>& a, const Vector2<int>& b, const Colour& colour)
{
    // Draw a line into the frame buffer. Assumes that this line need to be displayed and
    // gives the points maximum depth in the depth buffer.

    // Iterate over x or y depending on which gives the most smooth line.
    if (Math::Abs(a.y - b.y) < Math::Abs(a.x - b.x)) {
        // Iterate over x and find y by y = c + s*x.
        Real slope = static_cast<Real>(b.y - a.y)/static_cast<Real>(b.x - a.x);
        Real constant = a.y - slope*a.x;

        int xMin;
        int xMax;
        if (b.x < a.x) {
            xMin = b.x;
            xMax = a.x;
        }
        else {
            xMin = a.x;
            xMax = b.x;
        }

        if (xMin < 0) {
            xMin = 0;
        }
        if (mResolution.x < xMax) {
            xMax = mResolution.x;
        }

        for (int x = xMin; x < xMax; x++) {
            int y = constant + slope*x;
            if (y < 0 || mResolution.y <= y) {
                continue;
            }
            int index = x + y*mResolution.x;
            mDepthData[index] = realMin;
            PutPixel(4*index, colour.rgba.data());
        }
    }
    else {
        // Iterate over y and find x by x = c + s*y.
        Real slope = static_cast<Real>(b.x - a.x)/static_cast<Real>(b.y - a.y);
        Real constant = a.x - slope*a.y;

        int yMin;
        int yMax;
        if (b.y < a.y) {
            yMin = b.y;
            yMax = a.y;
        }
        else {
            yMin = a.y;
            yMax = b.y;
        }

        if (yMin < 0) {
            yMin = 0;
        }
        if (mResolution.y < yMax) {
            yMax = mResolution.y;
        }

        for (int y = yMin; y < yMax; y++) {
            int x = constant + slope*y;
            if (x < 0 || mResolution.x <= x) {
                continue;
            }
            int index = x + y*mResolution.x;
            mDepthData[index] = realMin;
            PutPixel(4*index, colour.rgba.data());
        }
    }
}


void Renderer::PutPixel(int index, const Uint8* colour)
{
    std::memcpy(mFrameBuffer + index, colour, 4);
}

