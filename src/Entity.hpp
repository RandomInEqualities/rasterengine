////////////////////////////////////////////////////////////////////////
//
// Entity.hpp
//
////////////////////////////////////////////////////////////////////////
#ifndef ENTITY_HPP_INCLUDED
#define ENTITY_HPP_INCLUDED

#include <string>
#include <vector>
#include <memory>

#include "Model.hpp"
#include "Time.hpp"
#include "Core/Types.hpp"
#include "Core/Vector3.hpp"
#include "Core/Quaternion.hpp"

class Instruction;

class Entity
{
public:

    // Default constructor sets the position to (0,0,0) and rotation to identity.
    Entity();

    // Copy and move construction and assignment.
    Entity(const Entity& entity);
    Entity& operator=(const Entity& entity);
    Entity(Entity&& entity) = default;
    Entity& operator=(Entity&& entity) = default;

    // The entity name, can be empty.
    std::string name;

    // The triangles in the entity.
    Model model;

    // The entity position and rotation in the scene.
    Vector3<Real> position;
    Quaternion rotation;

    // If true the entity should receive time updates, so it can process instructions.
    bool instructionsEnabled;

    // Any entity can move and rotate according to given instructions. There are a move and a
    // rotation instruction queue. An instruction is derived from the Instruction class.
    // This whole instruction system is not good and could f.x. be replaced by a node tree
    // system. This would be much more flexible and eliminate the need for the two separate
    // move and rotation queues.
    void MoveInstruction(std::unique_ptr<Instruction> i);
    void RotateInstruction(std::unique_ptr<Instruction> i);
    void ClearMoveInstructions();
    void ClearRotationInstructions();

    void Update(Seconds elapsed);

private:

    size_t mCurrentMove;
    size_t mCurrentRotation;
    std::vector<std::unique_ptr<Instruction>> mMoves;
    std::vector<std::unique_ptr<Instruction>> mRotations;

};


// Make an entity rotate around it self.
void RotateInLocalSpace(Entity& entity, Vector3<Real> axis, Real speed);


// Base instruction class.
class Instruction
{
public:
    // Update entity from this instruction.
    virtual void UpdateEntity(Entity& obj, Seconds elapsed) = 0;

    // Retrieve the next instruction. Returns index of the next instruction. Typically this is
    // currentInstruction + 1. But it is user defined in derived classes.
    virtual size_t NextInstruction(size_t currentInstruction) = 0;

    // Return a new identical object located on the heap.
    virtual Instruction* Clone() const = 0;

    virtual ~Instruction() {};
};


class MoveInstruction final : public Instruction
{
public:
    MoveInstruction(Vector3<Real> postion = Vector3<Real>{0.0,0.0,0.0}, Real moveSpeed = 1.0);
    void UpdateEntity(Entity& obj, Seconds elapsed) override;
    size_t NextInstruction(size_t currentInstruction) override;
    MoveInstruction* Clone() const override;

    Vector3<Real> position;
    Real moveSpeed;
private:
    bool reachedEnd;
};


class RotateInstruction final : public Instruction
{
public:
    RotateInstruction();
    RotateInstruction(Quaternion rotation, Real angularSpeed);

    void UpdateEntity(Entity& obj, Seconds elapsed) override;
    size_t NextInstruction(size_t currentInstruction) override;
    RotateInstruction* Clone() const override;

    Quaternion rotation;
    Real angularSpeed;
private:
    bool reachedEnd;
};


class WaitInstruction final : public Instruction
{
public:
    WaitInstruction(Seconds waitTime = 0.0s);

    void UpdateEntity(Entity& obj, Seconds elapsed) override;
    size_t NextInstruction(size_t currentInstruction) override;
    WaitInstruction* Clone() const override;

    void SetWaitTime(Seconds time);
private:
    Seconds totalWaitTime;
    Seconds remainingWaitTime;
};


class RepeatInstruction final : public Instruction
{
public:
    RepeatInstruction(size_t repeatIndex = 0);

    void UpdateEntity(Entity& obj, Seconds elapsed) override;
    size_t NextInstruction(size_t currentInstruction) override;
    RepeatInstruction* Clone() const override;

    size_t repeatIndex;
};


class StopInstruction final : public Instruction
{
public:
    void UpdateEntity(Entity& obj, Seconds elapsed) override;
    size_t NextInstruction(size_t currentInstruction) override;
    StopInstruction* Clone() const override;
};


#endif // ENTITY_HPP_INCLUDED


















