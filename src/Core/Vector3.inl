////////////////////////////////////////////////////////////////////////
//
// Vector3.inl
//
////////////////////////////////////////////////////////////////////////


#include "Math.hpp"
#include "Debug.hpp"


////////////////////////////////////////////////////////////////////////
// Vector3<T> public methods
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T>::Vector3()
{

}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T>::Vector3(T X, T Y, T Z) :
x(X),
y(Y),
z(Z)
{

}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T> Vector3<T>::operator-() const
{
    return Vector3<T>{-x, -y, -z};
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T> Vector3<T>::operator-(const Vector3<T>& right) const
{
    return Vector3<T>{x-right.x, y-right.y, z-right.z};
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T> Vector3<T>::operator+(const Vector3<T>& right) const
{
    return Vector3<T>{x+right.x, y+right.y, z+right.z};
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T>& Vector3<T>::operator+=(const Vector3<T>& right)
{
    x += right.x;
    y += right.y;
    z += right.z;
    return *this;
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T>& Vector3<T>::operator-=(const Vector3<T>& right)
{
    x -= right.x;
    y -= right.y;
    z -= right.z;
    return *this;
}


////////////////////////////////////////////////////////////////////////
template<typename T>
T Vector3<T>::Length() const
{
    return Math::Sqrt(x*x + y*y + z*z);
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T>& Vector3<T>::Normalize()
{
    T length = Length();
    ASSERT(!Math::IsZero(length), "Can't normalize zero vector");
    if (!Math::IsEqual(length, 1.0)) {
        x /= length;
        y /= length;
        z /= length;
    }
    return *this;
}


////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector3<T> operator*(const Vector3<T>& left, C right)
{
    return Vector3<T>{
        left.x*right,
        left.y*right,
        left.z*right
    };
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector3<T> operator*(C left, const Vector3<T>& right)
{
    return Vector3<T>{
        left*right.x,
        left*right.y,
        left*right.z
    };
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector3<T> operator/(const Vector3<T>& left, C right)
{
    return Vector3<T>{
        left.x/right,
        left.y/right,
        left.z/right
    };
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector3<T>& operator*=(Vector3<T>& left, C right)
{
    left.x *= right;
    left.y *= right;
    left.z *= right;
    return left;
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector3<T>& operator/=(Vector3<T>& left, C right)
{
    left.x /= right;
    left.y /= right;
    left.z /= right;
    return left;
}


////////////////////////////////////////////////////////////////////////
template<typename T>
T DotProduct(const Vector3<T>& a, const Vector3<T>& b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector3<T> CrossProduct(const Vector3<T>& a, const Vector3<T>& b)
{
    T x = a.y*b.z - a.z*b.y;
    T y = a.z*b.x - a.x*b.z;
    T z = a.x*b.y - a.y*b.x;
    return Vector3<T>{x,y,z};
}
