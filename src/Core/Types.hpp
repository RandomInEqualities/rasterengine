////////////////////////////////////////////////////////////////////////
//
// Types.hpp - Core type definitions.
//
////////////////////////////////////////////////////////////////////////

#ifndef TYPES_HPP_INCLUDED
#define TYPES_HPP_INCLUDED

#include <cstdint>
#include <cfloat>
#include <cstddef>


// Floating-point type. The chosen type is one that gives optimal performance.
using Real = float;

// Threshold for when to assume that a Real is zero.
constexpr Real realZeroTolerance{1e-10};
constexpr Real realMin{-FLT_MAX};
constexpr Real realMax{FLT_MAX};

// Unsigned integer that can hold a pointer/array index.
using std::size_t;

// Integers with fixed-width.
using Int8 = std::int8_t;
using Int16 = std::int16_t;
using Int32 = std::int32_t;
using Int64 = std::int64_t;
using Uint8 = std::uint8_t;
using Uint16 = std::uint16_t;
using Uint32 = std::uint32_t;
using Uint64 = std::uint64_t;


#endif // TYPES_HPP_INCLUDED
