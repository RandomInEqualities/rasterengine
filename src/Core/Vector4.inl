////////////////////////////////////////////////////////////////////////
//
// Vector4.inl
//
////////////////////////////////////////////////////////////////////////


#include "Math.hpp"
#include "Debug.hpp"


////////////////////////////////////////////////////////////////////////
// Vector4<T> public methods
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T>::Vector4()
{

}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T>::Vector4(T X, T Y, T Z, T W) :
x(X),
y(Y),
z(Z),
w(W)
{

}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T> Vector4<T>::operator-() const
{
    return Vector4<T>{-x, -y, -z, -w};
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T> Vector4<T>::operator-(const Vector4<T>& right) const
{
    return Vector4<T>{x-right.x, y-right.y, z-right.z, w-right.w};
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T> Vector4<T>::operator+(const Vector4<T>& right) const
{
    return Vector4<T>{x+right.x, y+right.y, z+right.z, w+right.w};
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T>& Vector4<T>::operator+=(const Vector4<T>& right)
{
    x += right.x;
    y += right.y;
    z += right.z;
    w += right.w;
    return *this;
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T>& Vector4<T>::operator-=(const Vector4<T>& right)
{
    x -= right.x;
    y -= right.y;
    z -= right.z;
    w -= right.w;
    return *this;
}


////////////////////////////////////////////////////////////////////////
template<typename T>
T Vector4<T>::Length() const
{
    return Math::Sqrt(x*x + y*y + z*z + w*w);
}


////////////////////////////////////////////////////////////////////////
template<typename T>
Vector4<T>& Vector4<T>::Normalize()
{
    T length = Length();
    ASSERT(!Math::IsZero(length), "Can't normalize zero vector");
    if (!Math::IsEqual(length, 1.0)) {
        x /= length;
        y /= length;
        z /= length;
        w /= length;
    }
    return *this;
}


////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector4<T> operator*(const Vector4<T>& left, C right)
{
    return Vector4<T>{
        left.x*right,
        left.y*right,
        left.z*right,
        left.w*right
    };
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector4<T> operator*(C left, const Vector4<T>& right)
{
    return Vector4<T>{
        left*right.x,
        left*right.y,
        left*right.z,
        left*right.w
    };
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector4<T> operator/(const Vector4<T>& left, C right)
{
    return Vector4<T>{
        left.x/right,
        left.y/right,
        left.z/right,
        left.w/right
    };
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector4<T>& operator*=(Vector4<T>& left, C right)
{
    left.x *= right;
    left.y *= right;
    left.z *= right;
    left.w *= right;
    return left;
}


////////////////////////////////////////////////////////////////////////
template<typename T, typename C>
Vector4<T>& operator/=(Vector4<T>& left, C right)
{
    left.x /= right;
    left.y /= right;
    left.z /= right;
    left.w /= right;
    return left;
}

