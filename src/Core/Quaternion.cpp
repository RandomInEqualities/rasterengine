////////////////////////////////////////////////////////////////////////
//
// Quaternion.cpp
//
////////////////////////////////////////////////////////////////////////

#include "Quaternion.hpp"
#include "Types.hpp"
#include "Debug.hpp"
#include "Math.hpp"
#include "Vector3.hpp"


////////////////////////////////////////////////////////////////////////
//
// Quaternion public methods
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Quaternion::Quaternion() :
w(1.0),
x(0.0),
y(0.0),
z(0.0)
{

}


////////////////////////////////////////////////////////////////////////
Quaternion::Quaternion(const Vector3<Real>& axis, Real radians)
{
    SetupRotation(axis, radians);
}


////////////////////////////////////////////////////////////////////////
Real Quaternion::Angle() const
{
    return 2*Math::Acos(w);
}


////////////////////////////////////////////////////////////////////////
Vector3<Real> Quaternion::Axis() const
{
    // Find the rotation angle.
    Real angle = 2.0*Math::Acos(w);
    Real sinAngleOver2 = Math::Sin(angle/2.0);

    // If sinAngleOver2 is zero then we have no rotation. We return an arbitrary rotation axis.
    if (Math::IsZero(sinAngleOver2)) {
        return Vector3<Real>{1.0, 0.0, 0.0};
    }

    return Vector3<Real>{x/sinAngleOver2, y/sinAngleOver2, z/sinAngleOver2};
}


////////////////////////////////////////////////////////////////////////
Quaternion Quaternion::Inverse() const
{
    Quaternion result;
    result.w = w;
    result.x = -x;
    result.y = -y;
    result.z = -z;
    return result;
}


////////////////////////////////////////////////////////////////////////
Quaternion Quaternion::Exponentiation(Real exponent) const
{
    // We make sure that w != 1 so we do not have sin(alpha)=0. If we do have sin(alpha)=0 then
    // we just return this quaternion as we have no Real angular displacement anyway.
    if (Math::Abs(w) < 1.0-realZeroTolerance) {

        // If the quaternion q is equal to [cos(alpha) sin(alpha)*n], then it holds that
        // q^t = exp(t*log(q)) = [cos(alpha*t) sin(alpha*t)*n].
        Quaternion result;

        Real alpha = Math::Acos(w);
        result.w = Math::Cos(alpha*exponent);

        Real sinAlphaExponentOverSinAlpha = Math::Sin(alpha*exponent)/Math::Sin(alpha);

        result.x = sinAlphaExponentOverSinAlpha*x;
        result.y = sinAlphaExponentOverSinAlpha*y;
        result.z = sinAlphaExponentOverSinAlpha*z;

        return result;

    }
    else {
        return *this;
    }
}


////////////////////////////////////////////////////////////////////////
Quaternion& Quaternion::SetupIdentity()
{
    w = 1.0;
    x = y = z = 0.0;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quaternion& Quaternion::SetupRotation(Vector3<Real> axis, Real radians)
{
    Real length = axis.Length();
    ASSERT(!Math::IsZero(length), "Rotation axis is zero");

    // Make sure n is normalized. We check if it is normalized and if not normalize it.
    if (realZeroTolerance < Math::Abs(length-1.0)) {
        axis.x /= length;
        axis.y /= length;
        axis.z /= length;
    }

    // Assign the rotation axis and angle.
    Real sinAngleOver2 = Math::Sin(radians/2.0);
    x = sinAngleOver2*axis.x;
    y = sinAngleOver2*axis.y;
    z = sinAngleOver2*axis.z;
    w = Math::Cos(radians/2.0);
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quaternion& Quaternion::SetupRotationAroundXAxis(Real radians)
{
    w = Math::Cos(radians/2.0);
    x = Math::Sin(radians/2.0);
    y = 0.0;
    z = 0.0;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quaternion& Quaternion::SetupRotationAroundYAxis(Real radians)
{
    w = Math::Cos(radians/2.0);
    x = 0.0;
    y = Math::Sin(radians/2.0);
    z = 0.0;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quaternion& Quaternion::SetupRotationAroundZAxis(Real radians)
{
    w = Math::Cos(radians/2.0);
    x = 0.0;
    y = 0.0;
    z = Math::Sin(radians/2.0);
    return *this;
}


////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Quaternion operator*(const Quaternion& a, const Quaternion& b)
{
    Quaternion result;
    result.w = a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z;
    result.x = a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y;
    result.y = a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z;
    result.z = a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x;
    return result;
}


////////////////////////////////////////////////////////////////////////
Quaternion& operator*=(Quaternion& a, const Quaternion& b)
{
    a = b*a;
    return a;
}


////////////////////////////////////////////////////////////////////////
Real DotProduct(const Quaternion& a, const Quaternion& b)
{
    return a.w*b.w + a.x*b.x + a.y*b.y + a.z*b.z;
}


////////////////////////////////////////////////////////////////////////
Quaternion Slerp(Quaternion a, Quaternion b, Real t)
{
    ASSERT((-1.0 < t) && (t < 1.0), "t out of range");

    // We find the angle omega between the two quaternions. For this we use that cos(omega)
    // is DotProduct(a,b). If this is negative we switch sign on a, which ensures consistent
    // results.
    Real cosOmega = DotProduct(a,b);
    if (cosOmega < 0.0) {
        a.x = -a.x;
        a.y = -a.y;
        a.z = -a.z;
        a.w = -a.w;
        cosOmega = -cosOmega;
    }

    // We express the wanted quaternion in the basis of a and b. This gives a nice expression
    // where result = k0*a + k1*b.
    Real k0, k1;
    if (cosOmega > 1.0-realZeroTolerance) {
        // We use Linear Interpolation here result = a + t*(b-a).
        k0 = 1.0 - t;
        k1 = t;
    }
    else {
        // We use Spherical Linear Interpolation.
        Real omega = Math::Acos(cosOmega);
        Real oneOverSinOmega = 1.0/Math::Sin(omega);
        k0 = oneOverSinOmega*Math::Sin(omega*(1.0 - t));
        k1 = oneOverSinOmega*Math::Sin(omega*t);
    }

    Quaternion result;
    result.w = k0*a.w + k1*b.w;
    result.x = k0*a.x + k1*b.x;
    result.y = k0*a.y + k1*b.y;
    result.z = k0*a.z + k1*b.z;
    return result;
}


