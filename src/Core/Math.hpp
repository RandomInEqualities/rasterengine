//////////////////////////////////////////////////////////////////////////
//
// Math.hpp - Essential math utilities.
//
//////////////////////////////////////////////////////////////////////////

#ifndef MATH_HPP_INCLUDED
#define MATH_HPP_INCLUDED

#include "Types.hpp"


namespace Math
{


    constexpr Real pi{3.141592653589793};
    constexpr Real piOver2{pi/2.0};
    constexpr Real piOver4{pi/4.0};

    // Perform equality tests on floating-points. Uses Real's zero tolerance constant.
    bool IsZero(Real x);
    bool IsEqual(Real x, Real y);

    // Clamp a value to be between min and max.
    template<typename A, typename B, typename C>
    A Clamp(const A& value, const B& min, const C& max);

    Real Abs(Real x);
    Real Sqrt(Real x);

    // Round to nearest whole number.
    Real Round(Real x);

    // Compute radians from degrees and the other way.
    Real RadToDeg(Real radian);
    Real DegToRad(Real degree);

    // Trigonometric functions. The accept radians.
    Real Sin(Real radian);
    Real Cos(Real radian);
    Real Tan(Real radian);

    // Inverse trigonometric functions. If x is outside [-1,1] we clamp x into the interval.
    Real Acos(Real x);
    Real Asin(Real x);
    Real Atan(Real x);


} // namespace Math

#include "Math.inl"


#endif // MATH_HPP_INCLUDED
