////////////////////////////////////////////////////////////////////////
//
// Matrix3x3.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef MATRIX3X3_HPP_INCLUDED
#define MATRIX3X3_HPP_INCLUDED

#include "Types.hpp"
template<typename T> class Vector3;
class Quaternion;
class Matrix4x4;


class Matrix3x3
{
public:

    Real Determinant() const;
    Matrix3x3 Inverse() const;

    // Setup the identity matrix.
    Matrix3x3& SetupIdentity();

    // Setup a rotation matrix.
    Matrix3x3& SetupRotation(Vector3<Real> axis, Real radians);
    Matrix3x3& SetupRotation(const Quaternion& q);

    // Setup scale along an axis.
    Matrix3x3& SetupScale(Vector3<Real> axis, Real scale);

    // Convert this matrix to a 4x4 matrix.
    Matrix4x4 As4x4();

    // The 12 matrix elements.
    Real m00, m01, m02;
    Real m10, m11, m12;
    Real m20, m21, m22;

};

// Matrix3x3-Matrix3x3 multiplication.
Matrix3x3 operator*(const Matrix3x3& left, const Matrix3x3& right);
Matrix3x3& operator*=(Matrix3x3& left, const Matrix3x3& right);

// Matrix3x3-Vector3 multiplication.
Vector3<Real> operator*(const Matrix3x3& left, const Vector3<Real>& right);
Vector3<Real>& operator*=(Vector3<Real>& left, const Matrix3x3& right);

// Matrix3x3-Matrix4x4 multiplication. Provided because they can save a few computations.
Matrix4x4 Multiply(const Matrix3x3& left, const Matrix4x4& right);
Matrix4x4 Multiply(const Matrix4x4& left, const Matrix3x3& right);


#endif // MATRIX3X3_HPP_INCLUDED


