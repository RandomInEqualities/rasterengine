////////////////////////////////////////////////////////////////////////
//
// Quaternion.hpp - Declaration of a quaternion class to represent 3D rotations.
//
////////////////////////////////////////////////////////////////////////

#ifndef QUATERNION_HPP_INCLUDED
#define QUATERNION_HPP_INCLUDED

#include "Types.hpp"
template<typename T> class Vector3;


class Quaternion
{
public:

    // Constructor sets up the identity quaternion or a rotation from an axis and angle.
    Quaternion();
    Quaternion(const Vector3<Real>& axis, Real radians);

    // The rotation angle in radians.
    Real Angle() const;

    // The normalized rotation axis.
    Vector3<Real> Axis() const;

    // The inverse quaternion. Makes the quaternion rotate in the opposite direction.
    Quaternion Inverse() const;

    // Quaternion exponentiation. With exponentiation we can take a fraction of a quaternion.
    // For example a fourth of the quaternion rotation can be retrieved with exponent=0.25.
    Quaternion Exponentiation(Real exponent) const;

    // Set the quaternion to the identity (1, (0,0,0)).
    Quaternion& SetupIdentity();

    // Construct the quaternion from an angle and a rotation axis.
    Quaternion& SetupRotation(Vector3<Real> axis, Real radians);
    Quaternion& SetupRotationAroundXAxis(Real radians);
    Quaternion& SetupRotationAroundYAxis(Real radians);
    Quaternion& SetupRotationAroundZAxis(Real radians);

    // Four members w,x,y,z. If a rotation phi is around a normalized axis r=(n,m,o)
    // then w = cos(phi/2) and (x,y,z) = sin(phi/2)*r.
    Real w;
    Real x;
    Real y;
    Real z;

};

// Multiplication represent the quaternion cross product. The cross product can be used to combine
// rotation quaternions together.
Quaternion operator*(const Quaternion& left, const Quaternion& right);
Quaternion& operator*=(Quaternion& left, const Quaternion& right);

// Quaternion dot product. The dotproduct is similar to a vectors dotproduct.
Real DotProduct(const Quaternion& left, const Quaternion& right);

// Spherical Linear Interpolation. Returns a quaternion that has a rotation, which is between
// quaternion a and b's rotation. The resulting rotation is the fraction t from a to b. For
// example with t=0.25 the returned quaternion is a fourth of the way between a and b. The
// parameter t is assumed to be between 0.0 and 1.0.
Quaternion Slerp(Quaternion a, Quaternion b, Real t);


#endif // QUATERNION_HPP_INCLUDED
