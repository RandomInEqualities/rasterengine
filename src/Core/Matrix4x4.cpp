////////////////////////////////////////////////////////////////////////
//
// Matrix4x4.cpp
//
////////////////////////////////////////////////////////////////////////

#include "Matrix4x4.hpp"
#include "Types.hpp"
#include "Debug.hpp"
#include "Math.hpp"
#include "Quaternion.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"


////////////////////////////////////////////////////////////////////////
//
// Matrix4x4 public methods
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Real Matrix4x4::Determinant() const
{
    return
        m00*(m11*(m22*tw-m32*tz) - m12*(m21*tw-m31*tz) + ty*(m21*m32-m22*m31)) -
        m01*(m10*(m22*tw-m32*tz) - m12*(m20*tw-m30*tz) + ty*(m20*m32-m22*m30)) +
        m02*(m10*(m21*tw-m31*tz) - m11*(m20*tw-m30*tz) + ty*(m20*m31-m30*m21)) -
        tx*(m10*(m21*m32-m31*m22) - m11*(m20*m32-m30*m22) + m12*(m20*m31-m30*m21));
}


////////////////////////////////////////////////////////////////////////
Matrix4x4 Matrix4x4::Inverse() const
{
    Real determinant = Determinant();
    if (Math::IsZero(determinant)) {
        ASSERT(false, "Singular matrix");
        return *this;
    }

    // Calculate the inverse matrix the hard(coded) way.
    Matrix4x4 result;
    result.m00 = m12*tz*m31 - ty*m22*m31 + ty*m21*m32 - m11*tz*m32 - m12*m21*tw + m11*m22*tw;
    result.m01 = tx*m22*m31 - m02*tz*m31 - tx*m21*m32 + m01*tz*m32 + m02*m21*tw - m01*m22*tw;
    result.m02 = m02*ty*m31 - tx*m12*m31 + tx*m11*m32 - m01*ty*m32 - m02*m11*tw + m01*m12*tw;
    result.tx  = tx*m12*m21 - m02*ty*m21 - tx*m11*m22 + m01*ty*m22 + m02*m11*tz - m01*m12*tz;

    result.m10 = ty*m22*m30 - m12*tz*m30 - ty*m20*m32 + m10*tz*m32 + m12*m20*tw - m10*m22*tw;
    result.m11 = m02*tz*m30 - tx*m22*m30 + tx*m20*m32 - m00*tz*m32 - m02*m20*tw + m00*m22*tw;
    result.m12 = tx*m12*m30 - m02*ty*m30 - tx*m10*m32 + m00*ty*m32 + m02*m10*tw - m00*m12*tw;
    result.ty  = m02*ty*m20 - tx*m12*m20 + tx*m10*m22 - m00*ty*m22 - m02*m10*tz + m00*m12*tz;

    result.m20 = m11*tz*m30 - ty*m21*m30 + ty*m20*m31 - m10*tz*m31 - m11*m20*tw + m10*m21*tw;
    result.m21 = tx*m21*m30 - m01*tz*m30 - tx*m20*m31 + m00*tz*m31 + m01*m20*tw - m00*m21*tw;
    result.m22 = m01*ty*m30 - tx*m11*m30 + tx*m10*m31 - m00*ty*m31 - m01*m10*tw + m00*m11*tw;
    result.tz  = tx*m11*m20 - m01*ty*m20 - tx*m10*m21 + m00*ty*m21 + m01*m10*tz - m00*m11*tz;

    result.m30 = m12*m21*m30 - m11*m22*m30 - m12*m20*m31 + m10*m22*m31 + m11*m20*m32 - m10*m21*m32;
    result.m31 = m01*m22*m30 - m02*m21*m30 + m02*m20*m31 - m00*m22*m31 - m01*m20*m32 + m00*m21*m32;
    result.m32 = m02*m11*m30 - m01*m12*m30 - m02*m10*m31 + m00*m12*m31 + m01*m10*m32 - m00*m11*m32;
    result.tw  = m01*m12*m20 - m02*m11*m20 + m02*m10*m21 - m00*m12*m21 - m01*m10*m22 + m00*m11*m22;

    result.m00 /= determinant;
    result.m01 /= determinant;
    result.m02 /= determinant;
    result.tx  /= determinant;
    result.m10 /= determinant;
    result.m11 /= determinant;
    result.m12 /= determinant;
    result.ty  /= determinant;
    result.m20 /= determinant;
    result.m21 /= determinant;
    result.m22 /= determinant;
    result.tz  /= determinant;
    result.m30 /= determinant;
    result.m31 /= determinant;
    result.m32 /= determinant;
    result.tw  /= determinant;
    return result;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& Matrix4x4::SetupIdentity()
{
    m00 = 1.0, m01 = 0.0, m02 = 0.0, tx = 0.0;
    m10 = 0.0, m11 = 1.0, m12 = 0.0, ty = 0.0;
    m20 = 0.0, m21 = 0.0, m22 = 1.0, tz = 0.0;
    m30 = 0.0, m31 = 0.0, m32 = 0.0, tw = 1.0;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& Matrix4x4::SetupTranslation(const Vector3<Real>& t)
{
    SetupIdentity();
    tx = t.x;
    ty = t.y;
    tz = t.z;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& Matrix4x4::SetupRotation(Vector3<Real> axis, Real radians)
{
    tx = ty = tz = m30 = m31 = m32 = 0.0;
    tw = 1.0;

    Real sin = Math::Sin(radians);
    Real cos = Math::Cos(radians);
    Real oneMinusCos = 1.0-cos;
    ASSERT(!Math::IsZero(axis.Length()), "Rotation axis is zero");
    axis.Normalize();

    // From the standard rotation formula we compute the matrix elements.
    m00 = axis.x*axis.x*oneMinusCos + cos;
    m01 = axis.x*axis.y*oneMinusCos - axis.z*sin;
    m02 = axis.x*axis.z*oneMinusCos + axis.y*sin;

    m10 = axis.y*axis.x*oneMinusCos + axis.z*sin;
    m11 = axis.y*axis.y*oneMinusCos + cos;
    m12 = axis.y*axis.z*oneMinusCos - axis.x*sin;

    m20 = axis.z*axis.x*oneMinusCos - axis.y*sin;
    m21 = axis.z*axis.y*oneMinusCos + axis.x*sin;
    m22 = axis.z*axis.z*oneMinusCos + cos;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& Matrix4x4::SetupRotation(const Quaternion& q)
{
    tx = ty = tz = m30 = m31 = m32 = 0.0;
    tw = 1.0;

    // We use a derived formula to convert the quaternion to a rotation matrix.
    Real xx = 2.0*q.x*q.x;
    Real yy = 2.0*q.y*q.y;
    Real zz = 2.0*q.z*q.z;

    m00 = 1.0 - yy - zz;
    m01 = 2.0*q.x*q.y - 2.0*q.w*q.z;
    m02 = 2.0*q.x*q.z + 2.0*q.w*q.y;

    m10 = 2.0*q.x*q.y + 2.0*q.w*q.z;
    m11 = 1.0 - xx - zz;
    m12 = 2.0*q.y*q.z - 2.0*q.w*q.x;

    m20 = 2.0*q.x*q.z - 2.0*q.w*q.y;
    m21 = 2.0*q.y*q.z + 2.0*q.w*q.x;
    m22 = 1.0 - xx - yy;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& Matrix4x4::SetupScale(Vector3<Real> axis, Real scale)
{
    tx = ty = tz = m30 = m31 = m32 = 0.0;
    tw = 1.0;

    ASSERT(!Math::IsZero(axis.Length()), "Scale axis is zero");
    axis.Normalize();
    Real scaleMinusOne = scale - 1.0;

    m00 = 1.0 + scaleMinusOne*axis.x*axis.x;
    m01 = scaleMinusOne*axis.x*axis.y;
    m02 = scaleMinusOne*axis.x*axis.z;

    m10 = scaleMinusOne*axis.x*axis.y;
    m11 = 1.0 + scaleMinusOne*axis.y*axis.y;
    m12 = scaleMinusOne*axis.y*axis.z;

    m20 = scaleMinusOne*axis.x*axis.z;
    m21 = scaleMinusOne*axis.y*axis.z;
    m22 = 1.0 + scaleMinusOne*axis.z*axis.z;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& Matrix4x4::SetupLocalToParent(const Vector3<Real>& position, const Quaternion& rotation)
{
    // The local to parent transform is translationMatrix*rotationMatrix. As translation happens
    // last we can set rotation and translation independently.
    SetupRotation(rotation);
    tx = position.x;
    ty = position.y;
    tz = position.z;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& Matrix4x4::SetupParentToLocal(const Vector3<Real>& position, const Quaternion& rotation)
{
    // The parent to local transform is rotationMatrix*translationMatrix. As translation happens
    // first we must account this being rotated.
    Matrix4x4 matrixTranslation;
    matrixTranslation.SetupTranslation(-position);

    Matrix4x4 matrixRotation;
    matrixRotation.SetupRotation(rotation.Inverse());

    *this = matrixRotation*matrixTranslation;
    return *this;
}


////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Matrix4x4 operator*(const Matrix4x4& a, const Matrix4x4& b)
{
    Matrix4x4 result;
    result.m00 = a.m00*b.m00 + a.m01*b.m10 + a.m02*b.m20 + a.tx*b.m30;
    result.m01 = a.m00*b.m01 + a.m01*b.m11 + a.m02*b.m21 + a.tx*b.m31;
    result.m02 = a.m00*b.m02 + a.m01*b.m12 + a.m02*b.m22 + a.tx*b.m32;
    result.tx  = a.m00*b.tx + a.m01*b.ty + a.m02*b.tz + a.tx*b.tw;

    result.m10 = a.m10*b.m00 + a.m11*b.m10 + a.m12*b.m20 + a.ty*b.m30;
    result.m11 = a.m10*b.m01 + a.m11*b.m11 + a.m12*b.m21 + a.ty*b.m31;
    result.m12 = a.m10*b.m02 + a.m11*b.m12 + a.m12*b.m22 + a.ty*b.m32;
    result.ty  = a.m10*b.tx + a.m11*b.ty + a.m12*b.tz + a.ty*b.tw;

    result.m20 = a.m20*b.m00 + a.m21*b.m10 + a.m22*b.m20 + a.tz*b.m30;
    result.m21 = a.m20*b.m01 + a.m21*b.m11 + a.m22*b.m21 + a.tz*b.m31;
    result.m22 = a.m20*b.m02 + a.m21*b.m12 + a.m22*b.m22 + a.tz*b.m32;
    result.tz  = a.m20*b.tx + a.m21*b.ty + a.m22*b.tz + a.tz*b.tw;

    result.m30 = a.m30*b.m00 + a.m31*b.m10 + a.m32*b.m20 + a.tw*b.m30;
    result.m31 = a.m30*b.m01 + a.m31*b.m11 + a.m32*b.m21 + a.tw*b.m31;
    result.m32 = a.m30*b.m02 + a.m31*b.m12 + a.m32*b.m22 + a.tw*b.m32;
    result.tw  = a.m30*b.tx + a.m31*b.ty + a.m32*b.tz + a.tw*b.tw;

    return result;
}


////////////////////////////////////////////////////////////////////////
Matrix4x4& operator*=(Matrix4x4& left, const Matrix4x4& right)
{
    left = right*left;
    return left;
}


////////////////////////////////////////////////////////////////////////
Vector4<Real> operator*(const Matrix4x4& a, const Vector4<Real>& b)
{
    Vector4<Real> result;
    result.x = a.m00*b.x + a.m01*b.y + a.m02*b.z + a.tx*b.w;
    result.y = a.m10*b.x + a.m11*b.y + a.m12*b.z + a.ty*b.w;
    result.z = a.m20*b.x + a.m21*b.y + a.m22*b.z + a.tz*b.w;
    result.w = a.m30*b.x + a.m31*b.y + a.m32*b.z + a.tw*b.w;
    return result;
}


////////////////////////////////////////////////////////////////////////
Vector4<Real>& operator*=(Vector4<Real>& left, const Matrix4x4& right)
{
    left = right*left;
    return left;
}


////////////////////////////////////////////////////////////////////////
Vector4<Real> operator*(const Matrix4x4& a, const Vector3<Real>& b)
{
    Vector4<Real> result;
    result.x = a.m00*b.x + a.m01*b.y + a.m02*b.z + a.tx;
    result.y = a.m10*b.x + a.m11*b.y + a.m12*b.z + a.ty;
    result.z = a.m20*b.x + a.m21*b.y + a.m22*b.z + a.tz;
    result.w = a.m30*b.x + a.m31*b.y + a.m32*b.z + a.tw;
    return result;
}
