////////////////////////////////////////////////////////////////////////
//
// Vector3.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef VECTOR3_HPP_INCLUDED
#define VECTOR3_HPP_INCLUDED


template<typename T>
class Vector3
{
public:

    Vector3();
    Vector3(T X, T Y, T Z);

    Vector3<T> operator-() const;

    Vector3<T> operator-(const Vector3<T>& right) const;
    Vector3<T> operator+(const Vector3<T>& right) const;

    Vector3<T>& operator+=(const Vector3<T>& right);
    Vector3<T>& operator-=(const Vector3<T>& right);

    T Length() const;
    Vector3<T>& Normalize();

    T x;
    T y;
    T z;

};

// Division and multiplication with another class. Fails if the underlying C and T conversions
// does not exist.
template<typename T, typename C>
Vector3<T> operator*(const Vector3<T>& left, C right);

template<typename T, typename C>
Vector3<T> operator*(C left, const Vector3<T>& right);

template<typename T, typename C>
Vector3<T> operator/(const Vector3<T>& left, C right);

template<typename T, typename C>
Vector3<T>& operator*=(Vector3<T>& left, C right);

template<typename T, typename C>
Vector3<T>& operator/=(Vector3<T>& left, C right);

// Common 3D vector operations.
template<typename T>
T DotProduct(const Vector3<T>& a, const Vector3<T>& b);

template<typename T>
Vector3<T> CrossProduct(const Vector3<T>& a, const Vector3<T>& b);

#include "Vector3.inl"


#endif // VECTOR3_HPP_INCLUDED
