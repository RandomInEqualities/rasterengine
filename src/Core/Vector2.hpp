////////////////////////////////////////////////////////////////////////
//
// Vector2.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef VECTOR2_HPP_INCLUDED
#define VECTOR2_HPP_INCLUDED


template<typename T>
class Vector2
{
public:

    Vector2();
    Vector2(T X, T Y);

    Vector2<T> operator-() const;

    Vector2<T> operator-(const Vector2<T>& right) const;
    Vector2<T> operator+(const Vector2<T>& right) const;

    Vector2<T>& operator+=(const Vector2<T>& right);
    Vector2<T>& operator-=(const Vector2<T>& right);

    T Length() const;
    Vector2<T>& Normalize();

    T x;
    T y;

};

// Division and multiplication with another class. The underlying T and C operator functions must
// exist.
template<typename T, typename C>
Vector2<T> operator*(const Vector2<T>& left, C right);

template<typename T, typename C>
Vector2<T> operator*(C left, const Vector2<T>& right);

template<typename T, typename C>
Vector2<T> operator/(const Vector2<T>& left, C right);

template<typename T, typename C>
Vector2<T>& operator*=(Vector2<T>& left, C right);

template<typename T, typename C>
Vector2<T>& operator/=(Vector2<T>& left, C right);

#include "Vector2.inl"


#endif // VECTOR2_HPP_INCLUDED
