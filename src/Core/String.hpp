////////////////////////////////////////////////////////////////////////
//
// String.hpp - Functions for easier string manipulation.
//
////////////////////////////////////////////////////////////////////////

#ifndef STRING_HPP_INCLUDED
#define STRING_HPP_INCLUDED

#include <string>
#include <vector>


// Make use of the s string literal, such that we can write "C++ style string!"s.
using namespace std::literals::string_literals;

// Convert a string to lower or upper case.
std::string ToLower(const std::string& str);
std::string ToUpper(const std::string& str);

// Extract the filename from a file path with '/' or '\' folder separation.
std::string StripPath(const std::string& filePath);

// Get the current time and date.
std::string GetTimeAndDate(char separator = ' ');

// Split a string into words.
std::vector<std::string> SplitIntoWords(const std::string& str);


#endif // STRING_HPP_INCLUDED
