////////////////////////////////////////////////////////////////////////
//
// String.cpp
//
////////////////////////////////////////////////////////////////////////


#include <string>
#include <vector>
#include <ctime>
#include <cctype>
#include <sstream>
#include "String.hpp"


////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
std::string ToLower(const std::string& str)
{
    std::string result;
    for (char ch : str) {
        result.push_back(std::tolower(ch));
    }
    return result;
}


////////////////////////////////////////////////////////////////////////
std::string ToUpper(const std::string& str)
{
    std::string result;
    for (char ch : str) {
        result.push_back(std::toupper(ch));
    }
    return result;
}


////////////////////////////////////////////////////////////////////////
std::string StripPath(const std::string& filePath)
{
    std::string name;
    for (char ch : filePath) {
        if (ch == '/' || ch == '\\') {
            name.clear();
        }
        else {
            name.push_back(ch);
        }
    }
    return name;
}


////////////////////////////////////////////////////////////////////////
std::string GetTimeAndDate(char separator)
{
    std::time_t time = std::time(nullptr);
    std::string timeString(std::ctime(&time));

    // Remove a potential newline at the end of the string.
    if (timeString.back() == '\n') {
        timeString.erase(timeString.size() - 1, 1);
    }

    for (char& ch : timeString) {

        // Remove ':' from string, so we can use it as a filename on windows.
        if (ch == ':') {
            ch = '.';
        }

        // Replace white-space by the separator.
        if (separator != ' ' && ch == ' ') {
            ch = separator;
        }
    }

    return timeString;
}


////////////////////////////////////////////////////////////////////////
std::vector<std::string> SplitIntoWords(const std::string& str)
{
    std::vector<std::string> words;
    std::istringstream stream(str);
    std::string word;
    while (stream >> word) {
        if (!word.empty()) {
            words.push_back(word);
        }
    }
    return words;
}

