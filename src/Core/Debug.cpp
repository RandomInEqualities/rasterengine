////////////////////////////////////////////////////////////////////////
//
// Debug.cpp
//
////////////////////////////////////////////////////////////////////////

#include <string>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>

#include "Debug.hpp"
#include "String.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"
#include "Quaternion.hpp"
#include "Matrix3x3.hpp"
#include "Matrix4x4.hpp"


namespace
{

// Control assertion failures and error messages.
const bool LogToConsole = true;
const bool LogToFile = true;
const std::string LogFileName = "Debug.txt";

} // namespace


////////////////////////////////////////////////////////////////////////
//
// Debug functions
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
void Log(const std::string& message)
{
    std::string time = GetTimeAndDate();
    if (LogToConsole) {
        std::cout << "\n" << time << ". " << message << std::endl;
    }

    if (LogToFile) {
        std::ofstream logFile;
        logFile.open(LogFileName, std::ios::out|std::ios::app);
        logFile << "\n" << time << ". " << message << std::endl;
        logFile.close();
    }
}


////////////////////////////////////////////////////////////////////////
void Assert(bool condition, const std::string& message, const std::string& file, int line)
{
    if (condition) {
        return;
    }

    std::string assertFailure = "Assert failure. ";
    if (!message.empty()) {
        assertFailure += message + ". ";
    }
    if (!file.empty()) {
        assertFailure += "File: " + StripPath(file) + "[Line " + std::to_string(line) + "]";
    }

    Log(assertFailure);
    std::abort();
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Vector2<Real>& vector)
{
    stream << vector.x << " " << vector.y;
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Vector3<Real>& vector)
{
    stream << vector.x << " " << vector.y << " " << vector.z;
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Vector4<Real>& vector)
{
    stream << vector.x << " " << vector.y << " " << vector.z << " " << vector.w;
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Quaternion& q)
{
    stream << q.w << " " << q.x << " " << q.y << " " << q.z;
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Matrix3x3& m)
{
    auto flags = stream.flags();
    auto precision = stream.precision();
    stream.precision(3);
    stream.setf(std::ios::fixed);
    stream.setf(std::ios::right);

    using std::setw;
    size_t w = 10;
    stream << "\n"
        << setw(w) << m.m00 << setw(w) << m.m01 << setw(w) << m.m02 << "\n"
        << setw(w) << m.m10 << setw(w) << m.m11 << setw(w) << m.m12 << "\n"
        << setw(w) << m.m20 << setw(w) << m.m21 << setw(w) << m.m22 << "\n";

    stream.precision(precision);
    stream.flags(flags);
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Matrix4x4& m)
{
    auto flags = stream.flags();
    auto precision = stream.precision();
    stream.precision(3);
    stream.setf(std::ios::fixed);
    stream.setf(std::ios::right);

    using std::setw;
    size_t w = 10;
    stream << "\n"
        << setw(w) << m.m00 << setw(w) << m.m01 << setw(w) << m.m02  << setw(w) << m.tx << "\n"
        << setw(w) << m.m10 << setw(w) << m.m11 << setw(w) << m.m12  << setw(w) << m.ty << "\n"
        << setw(w) << m.m20 << setw(w) << m.m21 << setw(w) << m.m22  << setw(w) << m.tz << "\n"
        << setw(w) << m.m30 << setw(w) << m.m31 << setw(w) << m.m32  << setw(w) << m.tw << "\n";

    stream.precision(precision);
    stream.flags(flags);
    return stream;
}
