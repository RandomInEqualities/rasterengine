////////////////////////////////////////////////////////////////////////
//
// Debug.hpp - Functions and MACROS to help with errors and debugging.
//
////////////////////////////////////////////////////////////////////////

#ifndef DEBUG_HPP_INCLUDED
#define DEBUG_HPP_INCLUDED

#include <string>
#include <iosfwd>
#include "Types.hpp"


// Assert macro. Does nothing if DEBUG is not defined.
#ifdef DEBUG
    #define ASSERT(condition, message) Assert((condition), (message), __FILE__, __LINE__)
#else
    #define ASSERT(condition, message) ((void)0)
#endif

// Log a message. Adds the time and date to the message.
void Log(const std::string& message);

// Abort program if condition is false. Logs message, file and line.
void Assert(bool condition, const std::string& message = "", const std::string& file = "",
            int line = 0);

// Print vectors, quaternions and matrices to ostreams.
class Quaternion;
class Matrix3x3;
class Matrix4x4;
template<typename T> class Vector2;
template<typename T> class Vector3;
template<typename T> class Vector4;
std::ostream& operator<<(std::ostream& stream, const Vector2<Real>& vector);
std::ostream& operator<<(std::ostream& stream, const Vector3<Real>& vector);
std::ostream& operator<<(std::ostream& stream, const Vector4<Real>& vector);
std::ostream& operator<<(std::ostream& stream, const Quaternion& q);
std::ostream& operator<<(std::ostream& stream, const Matrix3x3& m);
std::ostream& operator<<(std::ostream& stream, const Matrix4x4& m);


#endif // DEBUG_HPP_INCLUDED
