////////////////////////////////////////////////////////////////////////
//
// Vector4.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef VECTOR4_HPP_INCLUDED
#define VECTOR4_HPP_INCLUDED


template<typename T>
class Vector4
{
public:

    Vector4();
    Vector4(T X, T Y, T Z, T W);

    Vector4<T> operator-() const;

    Vector4<T> operator-(const Vector4<T>& right) const;
    Vector4<T> operator+(const Vector4<T>& right) const;

    Vector4<T>& operator+=(const Vector4<T>& right);
    Vector4<T>& operator-=(const Vector4<T>& right);

    T Length() const;
    Vector4<T>& Normalize();

    T x;
    T y;
    T z;
    T w;

};

// Division and multiplication with another class. Fails if the underlying C and T conversions
// does not exist.
template<typename T, typename C>
Vector4<T> operator*(const Vector4<T>& left, C right);

template<typename T, typename C>
Vector4<T> operator*(C left, const Vector4<T>& right);

template<typename T, typename C>
Vector4<T> operator/(const Vector4<T>& left, C right);

template<typename T, typename C>
Vector4<T>& operator*=(Vector4<T>& left, C right);

template<typename T, typename C>
Vector4<T>& operator/=(Vector4<T>& left, C right);

#include "Vector4.inl"


#endif // VECTOR4_HPP_INCLUDED
