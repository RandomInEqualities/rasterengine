////////////////////////////////////////////////////////////////////////
//
// Entity.cpp
//
////////////////////////////////////////////////////////////////////////


#include <string>
#include <vector>
#include <memory>

#include "Entity.hpp"
#include "Model.hpp"
#include "Time.hpp"

#include "Core/Types.hpp"
#include "Core/Math.hpp"
#include "Core/Vector3.hpp"
#include "Core/Quaternion.hpp"


////////////////////////////////////////////////////////////////////////
//
// Entity class methods
//
////////////////////////////////////////////////////////////////////////


Entity::Entity() :
position(0.0, 0.0, 0.0),
instructionsEnabled(false),
mCurrentMove(0),
mCurrentRotation(0)
{

}


Entity::Entity(const Entity& entity) :
name(entity.name),
model(entity.model),
position(entity.position),
rotation(entity.rotation),
instructionsEnabled(entity.instructionsEnabled),
mCurrentMove(entity.mCurrentMove),
mCurrentRotation(entity.mCurrentRotation)
{
    for (const auto& ptr : entity.mMoves) {
        mMoves.emplace_back(ptr->Clone());
    }
    for (const auto& ptr : entity.mRotations) {
        mRotations.emplace_back(ptr->Clone());
    }
}


Entity& Entity::operator=(const Entity& entity)
{
    if (this != &entity) {
        name = entity.name;
        model = entity.model;
        position = entity.position;
        rotation = entity.rotation;
        instructionsEnabled = entity.instructionsEnabled;

        mCurrentMove = entity.mCurrentMove;
        mCurrentRotation = entity.mCurrentRotation;

        mMoves.clear();
        for (const auto& ptr : entity.mMoves) {
            mMoves.emplace_back(ptr->Clone());
        }
        mRotations.clear();
        for (const auto& ptr : entity.mRotations) {
            mRotations.emplace_back(ptr->Clone());
        }
    }
    return *this;
}


void Entity::MoveInstruction(std::unique_ptr<Instruction> i)
{
    mMoves.push_back(std::move(i));
}


void Entity::RotateInstruction(std::unique_ptr<Instruction> i)
{
    mRotations.push_back(std::move(i));
}


void Entity::ClearMoveInstructions()
{
    mMoves.clear();
    mCurrentMove = 0;
}


void Entity::ClearRotationInstructions()
{
    mRotations.clear();
    mCurrentRotation = 0;
}


void Entity::Update(Seconds elapsed)
{
    if (!instructionsEnabled) {
        return;
    }

    if (mCurrentMove < mMoves.size()) {
        mMoves[mCurrentMove]->UpdateEntity(*this, elapsed);
        mCurrentMove = mMoves[mCurrentMove]->NextInstruction(mCurrentMove);
    }

    if (mCurrentRotation < mRotations.size()) {
        mRotations[mCurrentRotation]->UpdateEntity(*this, elapsed);
        mCurrentRotation = mRotations[mCurrentRotation]->NextInstruction(mCurrentRotation);
    }
}


////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////


void RotateInLocalSpace(Entity& entity, Vector3<Real> axis, Real speed)
{
    Quaternion rotation1, rotation2, rotation3;
    rotation1.SetupRotation(axis, 0);
    rotation2.SetupRotation(axis, 2*Math::pi/3);
    rotation3.SetupRotation(axis, 4*Math::pi/3);
    entity.RotateInstruction(std::make_unique<RotateInstruction>(rotation1, speed));
    entity.RotateInstruction(std::make_unique<RotateInstruction>(rotation2, speed));
    entity.RotateInstruction(std::make_unique<RotateInstruction>(rotation3, speed));
    entity.RotateInstruction(std::make_unique<RepeatInstruction>());
    entity.instructionsEnabled = true;
}


////////////////////////////////////////////////////////////////////////
//
// Move Instruction class
//
////////////////////////////////////////////////////////////////////////


MoveInstruction::MoveInstruction(Vector3<Real> pos, Real speed)
{
    position = pos;
    moveSpeed = speed;
    reachedEnd = false;
}


void MoveInstruction::UpdateEntity(Entity& obj, Seconds elapsed)
{
    Vector3<Real> moveVector = position - obj.position;
    Real distance = moveVector.Length();
    Real moveDistance = moveSpeed*elapsed.count();

    if (moveDistance < distance) {
        moveVector.Normalize();
        obj.position += moveVector*moveSpeed*elapsed.count();
    }
    else {
        obj.position = position;
        reachedEnd = true;
    }
}


size_t MoveInstruction::NextInstruction(size_t currentInstruction)
{
    if (reachedEnd) {
        reachedEnd = false;
        return currentInstruction + 1;
    }
    return currentInstruction;
}


MoveInstruction* MoveInstruction::Clone() const
{
    return new MoveInstruction(*this);
}


////////////////////////////////////////////////////////////////////////
//
// Rotate Instruction class
//
////////////////////////////////////////////////////////////////////////


RotateInstruction::RotateInstruction()
{
    rotation.SetupIdentity();
    angularSpeed = Math::pi/10.0;
    reachedEnd = false;
}


RotateInstruction::RotateInstruction(Quaternion rot, Real speed)
{
    rotation = rot;
    angularSpeed = speed;
    reachedEnd = false;
}


void RotateInstruction::UpdateEntity(Entity& obj, Seconds elapsed)
{
    Real angle = 2*Math::Acos(DotProduct(obj.rotation, rotation));
    Real moveAngle = angularSpeed*elapsed.count();

    if (moveAngle < angle && !Math::IsZero(angle)) {
        obj.rotation = Slerp(obj.rotation, rotation, moveAngle/angle);
    }
    else {
        obj.rotation = rotation;
        reachedEnd = true;
    }
}


size_t RotateInstruction::NextInstruction(size_t currentInstruction)
{
    if (reachedEnd) {
        reachedEnd = false;
        return currentInstruction + 1;
    }
    return currentInstruction;
}


RotateInstruction* RotateInstruction::Clone() const
{
    return new RotateInstruction(*this);
}


////////////////////////////////////////////////////////////////////////
//
// Wait Instruction class
//
////////////////////////////////////////////////////////////////////////


WaitInstruction::WaitInstruction(Seconds waitTime)
{
    SetWaitTime(waitTime);
}


void WaitInstruction::UpdateEntity(Entity&, Seconds elapsed)
{
    remainingWaitTime -= elapsed;
}


size_t WaitInstruction::NextInstruction(size_t currentInstruction)
{
    if (remainingWaitTime <= 0.0s) {
        return currentInstruction + 1;
    }
    return currentInstruction;
}


void WaitInstruction::SetWaitTime(Seconds time)
{
    totalWaitTime = time;
    remainingWaitTime = time;
}


WaitInstruction* WaitInstruction::Clone() const
{
    return new WaitInstruction(*this);
}


////////////////////////////////////////////////////////////////////////
//
// Repeat Instruction class
//
////////////////////////////////////////////////////////////////////////


RepeatInstruction::RepeatInstruction(size_t index)
{
    repeatIndex = index;
}


void RepeatInstruction::UpdateEntity(Entity&, Seconds)
{

}


size_t RepeatInstruction::NextInstruction(size_t)
{
    return repeatIndex;
}


RepeatInstruction* RepeatInstruction::Clone() const
{
    return new RepeatInstruction(*this);
}


////////////////////////////////////////////////////////////////////////
//
// Stop Instruction class
//
////////////////////////////////////////////////////////////////////////


void StopInstruction::UpdateEntity(Entity& obj, Seconds)
{
    obj.instructionsEnabled = false;
}


size_t StopInstruction::NextInstruction(size_t)
{
    return 0;
}


StopInstruction* StopInstruction::Clone() const
{
    return new StopInstruction(*this);
}

