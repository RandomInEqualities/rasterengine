#ifndef TIME_HPP_INCLUDED
#define TIME_HPP_INCLUDED


#include <chrono>
#include <ratio>
#include "Core/Types.hpp"

// Clock that we use to measure time.
using Clock = std::chrono::steady_clock;

// Representation of seconds and milliseconds.
using Seconds = std::chrono::duration<Real>;
using MilliSeconds = std::chrono::duration<Real, std::milli>;

// Alternatively you can use literals, for example 1.0s or 1.0ms.
using namespace std::literals::chrono_literals;

// A point in time.
using TimePoint = std::chrono::time_point<Clock>;


#endif // TIME_HPP_INCLUDED
