////////////////////////////////////////////////////////////////////////
//
// Renderer.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef RENDERER_HPP_INCLUDED
#define RENDERER_HPP_INCLUDED

#include <vector>
#include "Core/Types.hpp"
#include "Core/Matrix4x4.hpp"
#include "Core/Vector2.hpp"
#include "Model.hpp"
class Entity;
class RendererTriangle;


// Renderer renders an image. The renderer needs to know the entities to draw (and their textures
// if they have any), a view transform from world space to camera space, output resolution
// and zoom. When it have these it will simply output an RGBA image of the scene taken from the
// camera's perspective. It is a software renderer and it uses no external libraries.
class Renderer
{
public:

    Renderer();

    // Render entities into an image. The image must be large enough to hold a an RGBA
    // colour for each pixel in the given resolution.
    void Render(std::vector<Uint8>& output, const std::vector<Entity>& entities);

    // Specify the rendering parameters. Where we render from, the output resolution, zoom and
    // the textures that we use to colour the triangles.
    void SetViewTransform(const Matrix4x4& worldToCamera);
    void SetTextures(const TextureContainer* textures);
    void SetResolution(Vector2<unsigned int> resolution);
    void SetZoom(Vector2<Real> zoom);

    // By default we only draw triangle colours. You can change this here.
    void DrawColours(bool enabled);
    void DrawWireframes(bool enabled);
    bool IsDrawingColours() const;
    bool IsDrawingWireframes() const;

private:

    enum class ClipPlane
    {
        near,
        far,
        right,
        left,
        top,
        bottom
    };

    // The top level render function.
    void RenderTriangle(RendererTriangle& params);

    //
    //
    // Rendering functions
    //
    //

    // Compute camera to clip matrix (mCameraToClip). Uses mZoom, mNearZ and mFarZ.
    void ComputeCameraToClipMatrix();

    // Clear the frame and depth buffers.
    void ClearBuffers();

    // Clip triangle to view frustum. Returns false if the triangle is outside the view frustum
    // and if it should not be drawn. If the triangle is partly inside the view frustum it will
    // clip it, make new drawing calls and return false.
    bool ClipWithFrustrum(const RendererTriangle& params);

    // Clip a triangle with a plane. Makes new drawing calls for the one or two new triangles.
    void ClipWithPlane(const RendererTriangle& params, ClipPlane plane, bool p0cOutside,
        bool p1cOutside, bool p2cOutside);

    // Draw a triangle into the frameBuffer.
    void DrawTriangle(RendererTriangle& params);

    // Draw a horizontal line into the frame buffer. Draws at the given y value and from ab to cd.
    // The four points must be sorted as desired before calling.
    void ProcessScanLine(int y, const Vector2<int>& a, const Vector2<int>& b, const Vector2<int>& c,
        const Vector2<int>& d, const RendererTriangle& params);

    // Draw a point into the frame buffer. Interpolates the triangle texture if it has a texture.
    // b0, b1, b2 are barycentric eye space coordinates for the triangle point.
    void DrawPoint(int x, int y, Real z, Real b0, Real b1, Real b2, const RendererTriangle& params);

    // Draw a line from a to b into the frame buffer.
    void DrawLine(const Vector2<int>& a, const Vector2<int>& b, const Colour& colour);

    // Put a colour into the frame buffer.
    void PutPixel(int index, const Uint8* colour);

    //
    //
    // Scene parameters
    //
    //

    Uint8* mFrameBuffer;
    Vector2<int> mResolution;
    Vector2<int> mResolutionOver2;
    Vector2<Real> mZoom;
    const TextureContainer* mTextures;
    Matrix4x4 mCameraToClip;
    Matrix4x4 mWorldToCamera;

    //
    //
    // Rendering parameters
    //
    //

    bool mDrawBackFacingTriangles;
    bool mDrawColours;
    bool mDrawWireframes;
    Colour mWireframeColour;

    std::vector<Real> mDepthBuffer;

    // A pointer to the underlying depth buffer array. Stored for fast access.
    Real* mDepthData;

    // Far and near z planes.
    Real mNearZ;
    Real mFarZ;

};

#endif // RENDERER_HPP_INCLUDED
