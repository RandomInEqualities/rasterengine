////////////////////////////////////////////////////////////////////////
//
// ObjModel.cpp
//
////////////////////////////////////////////////////////////////////////


#include <vector>
#include <string>
#include <stdexcept>
#include <fstream>
#include <utility>
#include <ios>

#include "ObjModel.hpp"
#include "../Core/Types.hpp"
#include "../Core/String.hpp"
#include "../Core/Vector2.hpp"
#include "../Core/Vector3.hpp"


////////////////////////////////////////////////////////////////////////
//
// Private function declarations
//
////////////////////////////////////////////////////////////////////////


namespace Obj
{

// An object file face during the parsing phase. After the parsing phase we can convert it into
// a Face without index locations.
struct IndexFace
{
    // Index locations of the face vertices.
    std::vector<int> vertices;

    // Optional index locations of texture positions and normals.
    std::vector<int> texturePositions;
    std::vector<int> normals;

    // Optional group and material.
    std::string group;
    std::string material;
};

// Convert a string to a Real or int. Throws logic_error if it can not convert the whole string.
Real StringToReal(const std::string& str);
int StringToInt(const std::string& str);

// Try to read a vertex string into a face structure. Throws logic_error on failure.
void StringToVertex(std::string str, IndexFace& face);

// The face needs to be balanced such that there are either a normal for each vertex or
// zero normals. The same goes for the texture positions, zero or as many as there are vertices.
// Throws logic_error if face is unbalanced.
void CheckFaceBalance(const IndexFace& face);

// Function for easily throwing a runtime_error to signal a syntax error in the parsing of a file.
void ThrowRuntimeError(const std::string& filename, int line);

}


////////////////////////////////////////////////////////////////////////
//
// Material methods
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
void Obj::Material::Reset()
{
    name.clear();
    ambient = Vector3<Real>(0.0, 0.0, 0.0);
    diffuse = Vector3<Real>(0.0, 0.0, 0.0);
    specular = Vector3<Real>(0.0, 0.0, 0.0);
    transparency = 1.0;
    illumination = 0;
    ambientFilename.clear();
    diffuseFilename.clear();
    specularFilename.clear();
}


////////////////////////////////////////////////////////////////////////
//
// Model methods
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
void Obj::Model::ReadFromFile(const std::string& filename)
{
    // GCC 4.9.1 has not implemented that IO exceptions derive from runtime_error. Until it is
    // fixed we catch the IO errors and rethrow them as runtime_error.
    try {
        ReadFromFileImplementation(filename);
    }
    catch (const std::ios::failure& error) {
        throw std::runtime_error(error.what());
    }
}


////////////////////////////////////////////////////////////////////////
void Obj::Model::ReadFromFileImplementation(const std::string& filename)
{
    name.clear();
    faces.clear();
    materials.clear();

    std::ifstream file;
    file.open(filename);
    if (!file) {
        throw std::runtime_error(filename + " unable to open.");
    }
    file.exceptions(std::ifstream::badbit);

    // We use a subset of the object file format, see ObjModel.hpp for more info or
    // http://paulbourke.net/dataformats/obj/ for a full object file specification.

    // During file reading we load data into temporary structures that are converted to faces,
    // when everything has been read.
    std::vector<IndexFace> indexFaces;
    std::vector<Vector3<Real>> vertices;
    std::vector<Vector3<Real>> normals;
    std::vector<Vector2<Real>> texturePositions;

    // The currently active group and material.
    std::string group;
    std::string material;

    int lineNumber = 0;
    std::string line;
    while (std::getline(file, line)) {

        lineNumber++;

        std::vector<std::string> words = SplitIntoWords(line);
        if (words.empty()) {
            continue;
        }
        words[0] = ToLower(words[0]);

        if (words[0] == "v") {

            if (words.size() != 4 && words.size() != 5) {
                ThrowRuntimeError(filename, lineNumber);
            }
            try {
                Real x = StringToReal(words[1]);
                Real y = StringToReal(words[2]);
                Real z = StringToReal(words[3]);
                vertices.emplace_back(x, y, z);
            }
            catch (std::logic_error) {
                ThrowRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "vn") {

            if (words.size() != 4) {
                ThrowRuntimeError(filename, lineNumber);
            }
            try {
                Real x = StringToReal(words[1]);
                Real y = StringToReal(words[2]);
                Real z = StringToReal(words[3]);
                normals.emplace_back(x, y, z);
            }
            catch (std::logic_error) {
                ThrowRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "vt") {

            if (words.size() != 3 && words.size() != 4) {
                ThrowRuntimeError(filename, lineNumber);
            }
            try {
                Real x = StringToReal(words[1]);
                Real y = StringToReal(words[2]);
                texturePositions.emplace_back(x, y);
            }
            catch (std::logic_error) {
                ThrowRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "f") {

            if (words.size() < 4) {
                ThrowRuntimeError(filename, lineNumber);
            }
            IndexFace indexFace;
            try {
                for (size_t index = 1; index < words.size(); index++) {
                    StringToVertex(words[index], indexFace);
                }
                CheckFaceBalance(indexFace);
            }
            catch (std::logic_error) {
                ThrowRuntimeError(filename, lineNumber);
            }
            indexFace.group = group;
            indexFace.material = material;
            indexFaces.emplace_back(std::move(indexFace));

        }
        else if (words[0] == "mtllib") {
            for (size_t index = 1; index < words.size(); index++) {
                ReadMaterialFile(words[index]);
            }
        }
        else if (words[0] == "usemtl") {
            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            material = words[1];
        }
        else if (words[0] == "g") {
            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            group = words[1];
        }
        else if (words[0] == "o") {
            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            name = words[1];
        }
    }

    // Check that each face has an empty or existing material.
    for (const IndexFace& face : indexFaces) {
        if (!face.material.empty()) {
            try {
                GetMaterial(face.material);
            }
            catch (std::out_of_range) {
                throw std::runtime_error(filename + " invalid material " + face.material + ".");
            }
        }
    }

    // Convert the index faces into faces.
    faces.reserve(indexFaces.size());
    for (size_t pos = 0; pos < indexFaces.size(); pos++) {

        Face face;
        try {
            for (int index : indexFaces[pos].vertices) {
                face.vertices.emplace_back(vertices.at(index - 1));
            }
            for (int index : indexFaces[pos].normals) {
                face.normals.emplace_back(normals.at(index - 1));
            }
            for (int index : indexFaces[pos].texturePositions) {
                face.texturePositions.emplace_back(texturePositions.at(index - 1));
            }
        }
        catch (std::out_of_range) {
            std::string message = " face " + std::to_string(pos) + " has invalid index.";
            throw std::runtime_error(filename + message);
        }

        face.group = indexFaces[pos].group;
        face.material = indexFaces[pos].material;
        faces.emplace_back(std::move(face));

    }
}


////////////////////////////////////////////////////////////////////////
const Obj::Material& Obj::Model::GetMaterial(const std::string& material) const
{
    for (const Material& existing : materials) {
        if (material == existing.name) {
            return existing;
        }
    }
    throw std::out_of_range(name + " unable to find material " + material + ".");
}


////////////////////////////////////////////////////////////////////////
void Obj::Model::AddMaterial(const Obj::Material& material)
{
    if (material.name.empty()) {
        return;
    }

    // check that we do not have duplicates.
    for (Material& existing : materials) {
        if (material.name == existing.name) {
            existing = material;
            return;
        }
    }

    materials.push_back(material);
}


////////////////////////////////////////////////////////////////////////
void Obj::Model::ReadMaterialFile(const std::string& filename)
{
    std::ifstream file(filename);
    if (!file) {
        throw std::runtime_error(filename + " unable to open.");
    }
    file.exceptions(std::ifstream::badbit);

    // The mtl file format has a fairly simple specification. It is based on the Phong light
    // reflection model, but can easily be extended or used for other models. An mtl file
    // specification can be found at http://paulbourke.net/dataformats/mtl/.

    // The material that we are currently loading.
    Material material;

    std::string line;
    int lineNumber = 0;
    while (std::getline(file, line)) {
        lineNumber++;

        std::vector<std::string> words = SplitIntoWords(line);
        if (words.empty()) {
            continue;
        }
        words[0] = ToLower(words[0]);

        if (words[0] == "newmtl") {

            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            AddMaterial(material);
            material.Reset();
            material.name = words[1];

        }
        else if (words[0] == "ka" || words[0] == "kd" || words[0] == "ks") {

            if (words.size() != 4) {
                ThrowRuntimeError(filename, lineNumber);
            }

            Vector3<Real> colour;
            try {
                colour.x = StringToReal(words[1]);
                colour.y = StringToReal(words[2]);
                colour.z = StringToReal(words[3]);
            }
            catch (std::logic_error) {
                ThrowRuntimeError(filename, lineNumber);
            }

            if (words[0] == "ka") {
                material.ambient = colour;
            }
            else if (words[0] == "kd") {
                material.diffuse = colour;
            }
            else if (words[0] == "ks") {
                material.specular = colour;
            }

        }
        else if (words[0] == "tr" || words[0] == "d") {

            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            try {
                material.transparency = StringToReal(words[1]);
            }
            catch (std::logic_error) {
                ThrowRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "illum") {

            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            try {
                material.illumination = StringToInt(words[1]);
            }
            catch (std::logic_error) {
                ThrowRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "map_ka") {

            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            material.ambientFilename = words[1];

        }
        else if (words[0] == "map_kd") {

            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            material.diffuseFilename = words[1];

        }
        else if (words[0] == "map_ks") {

            if (words.size() != 2) {
                ThrowRuntimeError(filename, lineNumber);
            }
            material.specularFilename = words[1];

        }
    }
    AddMaterial(material);
}


////////////////////////////////////////////////////////////////////////
//
// Private functions
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Real Obj::StringToReal(const std::string& str)
{
    size_t end = 0;
    Real result = static_cast<Real>(std::stod(str, &end));
    if (end != str.size()) {
        throw std::invalid_argument("StringToReal: unable to convert.");
    }
    return result;
}


////////////////////////////////////////////////////////////////////////
int Obj::StringToInt(const std::string& str)
{
    size_t end = 0;
    int result = std::stoi(str, &end);
    if (end != str.size()) {
        throw std::invalid_argument("StringToInt: unable to convert.");
    }
    return result;
}


////////////////////////////////////////////////////////////////////////
void Obj::StringToVertex(std::string str, Obj::IndexFace& face)
{
    // An face string can have four forms v, v/vt, v/vt/vn and v//vn, where v = vertex,
    // vn = normal and vt = texture position.
    // ... read the vertex index.
    size_t readEnd = 0;
    face.vertices.emplace_back(std::stoi(str, &readEnd));
    if (readEnd == str.size()) {
        return;
    }
    if (str.at(readEnd) != '/') {
        throw std::invalid_argument("Bad vertex.");
    }

    // ... read the texture position index.
    if (str.at(readEnd + 1) != '/') {
        str.erase(0, readEnd + 1);
        face.texturePositions.emplace_back(std::stoi(str, &readEnd));
        if (readEnd == str.size()) {
            return;
        }
        if (str.at(readEnd) != '/') {
            throw std::invalid_argument("Bad vertex.");
        }
        str.erase(0, readEnd + 1);
    }
    else {
        str.erase(0, readEnd + 2);
    }

    // ... read the normal vector index.
    face.normals.emplace_back(std::stoi(str, &readEnd));
    if (readEnd != str.size()) {
        throw std::invalid_argument("Bad vertex.");
    }
}


////////////////////////////////////////////////////////////////////////
void Obj::CheckFaceBalance(const Obj::IndexFace& face)
{
    size_t vSize = face.vertices.size();
    size_t vnSize = face.normals.size();
    size_t vtSize = face.texturePositions.size();

    if (vnSize != 0 && vnSize != vSize) {
        throw std::logic_error("Unbalanced face");
    }
    if (vtSize != 0 && vtSize != vSize) {
        throw std::logic_error("Unbalanced face");
    }

    return;
}


////////////////////////////////////////////////////////////////////////
void Obj::ThrowRuntimeError(const std::string& filename, int line)
{
    throw std::runtime_error(filename + " syntax error at line " + std::to_string(line));
}
