////////////////////////////////////////////////////////////////////////
//
// ObjModel.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef OBJMODEL_HPP_INCLUDED
#define OBJMODEL_HPP_INCLUDED

#include <vector>
#include <string>
#include "../Core/Types.hpp"
#include "../Core/Vector2.hpp"
#include "../Core/Vector3.hpp"


/*
    Object files are used to hold geometry in ASCII format. They have the obj extension.
    Material template library files (mtl extension) are used to hold textures and Phong light
    model parameters.

    We provide a subset of the obj file specification, see http://paulbourke.net/dataformats/obj/
    for a full specification. Right now we look for vertices (v), faces (f), texture coordinates
    (vt), normals (vn), materials (usemtl, mtllib), group names (g) and object names (o). When
    mtllib is detected we load the the material file. In the material file we look for
    transparency (tr, d), ambient (ka, map_ka), diffuse (kd, map_kd), specular (ks, map_ks),
    illumination mode (illum) and of course new materials (newmtl).

    Model holds the data of a complete object file.
    Material holds the data of a single material.
    Face holds the data for a single face.
*/
namespace Obj
{

struct Face
{
    // The vertex positions. There can be an arbitrary amount of vertices.
    std::vector<Vector3<Real>> vertices;

    // Optional vertex normals. The size is either zero or vertices.size().
    std::vector<Vector3<Real>> normals;

    // Optional texture positions. The size is either zero or vertices.size(). The x and y values
    // are between 0.0 and 1.0.
    std::vector<Vector2<Real>> texturePositions;

    // Optional group and material.
    std::string group;
    std::string material;
};

struct Material
{
    // The material name. Faces reference this name.
    std::string name;

    // Material parameters. The ambient, diffuse, specular and transparency are between 0.0
    // and 1.0. The illumination specifies the desired illumination mode.
    Vector3<Real> ambient = Vector3<Real>(0.0, 0.0, 0.0);
    Vector3<Real> diffuse = Vector3<Real>(0.0, 0.0, 0.0);
    Vector3<Real> specular = Vector3<Real>(0.0, 0.0, 0.0);
    Real transparency = 1.0;
    int illumination = 0;

    // Optional textures. We hold a filename to the textures.
    std::string ambientFilename;
    std::string diffuseFilename;
    std::string specularFilename;

    // Reset the material to the default parameters.
    void Reset();
};

struct Model
{
public:

    // The object name.
    std::string name;

    // The geometries in the object.
    std::vector<Face> faces;

    // Get the material from its name. Throws out_of_range on failure.
    const Material& GetMaterial(const std::string& material) const;

    // Load the model from an obj file. Throws runtime_error on failure.
    void ReadFromFile(const std::string& filename);

private:

    // Add a new material to the model. Overwrites any stored material with the same name.
    void AddMaterial(const Material& material);

    // Try to read and load a material file. Throws runtime_error on failure.
    void ReadMaterialFile(const std::string& filename);

    void ReadFromFileImplementation(const std::string& filename);

    std::vector<Material> materials;

};

}


#endif // OBJMODEL_HPP_INCLUDED
