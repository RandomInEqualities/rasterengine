////////////////////////////////////////////////////////////////////////
//
// TgaImage.hpp
//
////////////////////////////////////////////////////////////////////////
#ifndef TGAIMAGE_HPP_INCLUDED
#define TGAIMAGE_HPP_INCLUDED

#include <string>
#include <vector>
#include "../Core/Types.hpp"


// Load TGA image into an RGBA array. Throws runtime_error on failure.
std::vector<Uint8> LoadTGAImage(std::string filename, unsigned int& width, unsigned int& height);

// Save RGBA array as a TGA image. Throws std::runtime_error on failure.
void SaveTGAImage(std::string filename, const std::vector<Uint8>& image, unsigned int width,
    unsigned int height);


#endif // TGAIMAGE_HPP_INCLUDED
