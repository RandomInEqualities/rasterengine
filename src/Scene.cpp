////////////////////////////////////////////////////////////////////////
//
// Scene.cpp
//
////////////////////////////////////////////////////////////////////////

#include <SFML/Graphics.hpp>

#include <string>
#include <vector>
#include <stdexcept>
#include <map>
#include <memory>
#include <random>

#include "Scene.hpp"
#include "Entity.hpp"
#include "Camera.hpp"
#include "Renderer.hpp"
#include "Time.hpp"

#include "Core/Types.hpp"
#include "Core/Debug.hpp"
#include "Core/Vector2.hpp"
#include "Core/Vector3.hpp"
#include "Core/Math.hpp"

#include <iostream>


////////////////////////////////////////////////////////////////////////
//
// Scene public methods
//
////////////////////////////////////////////////////////////////////////


Scene::Scene() :
mResolution(800, 800),
mFov(70.0, 70.0),
mWindowStyle(sf::Style::Default),
mWindowVsync(false)
{
    // Update the window texture, window sprite, renderer resolution and renderer fov.
    SetResolution(mResolution);

    // Set initial camera position.
    mCamera.Init(Vector3<Real>{-0.2, -0.2, 0.2}, Vector3<Real>{0,0,0.2}, Vector3<Real>{0,0,1});

    // Create scene entities and load their resources.
    std::vector<std::string> modelFiles = {
        "Data/Geometry/teapot.obj",
        "Data/Geometry/cube.obj",
        "Data/Geometry/humanoid_tri.obj",
        "Data/Geometry/skull.obj",
        "Data/Geometry/bunny.obj"
    };

    std::default_random_engine engine;
    std::uniform_real_distribution<float> rand(-0.2, 0.5);
    for (auto filename : modelFiles) {
        mSceneEntities.emplace_back();
        try {
            mSceneEntities.back().model.LoadFromFile(filename, mSceneTextures);
            mSceneEntities.back().position = Vector3<Real>{
                rand(engine), rand(engine), 0
            };
            mSceneEntities.back().rotation = Quaternion{Vector3<Real>{1, 0, 0}, Math::piOver2};
        }
        catch (const std::runtime_error& error) {
            Log(error.what());
            mSceneEntities.pop_back();
        }
    }

    mSceneEntities[0].position = Vector3<Real>{100, 100, -40};
    mSceneEntities[0].rotation = Quaternion(Vector3<Real>{0,0,1}, 0);
    RotateInLocalSpace(mSceneEntities[0], Vector3<Real>{0,0,1}, 1.5);
    mSceneEntities[1].position = Vector3<Real>{0, 4, 0};
    mSceneEntities[2].position = Vector3<Real>{10, 10, 0};
    mSceneEntities[2].rotation = Quaternion{Vector3<Real>{1, 0, 0}, Math::piOver4};
    mSceneEntities[3].position = Vector3<Real>{15, 15, 0};

    mRenderer.SetTextures(&mSceneTextures);
}


void Scene::Begin()
{
    // Initialize FPS text
    sf::Font fpsFont;
    fpsFont.loadFromFile("Data/Font/sansation.ttf");
    sf::Text fpsText("FPS: ...", fpsFont);
    fpsText.setFillColor(sf::Color::Blue);
    int frames = 0;
    TimePoint fpsLastUpdateTime = Clock::now();

    CreateWindow();

    // Engine time follows the global Clock defined in Time.hpp.
    mFrameStartTime = Clock::now();

    while (mWindow.isOpen()) {

        mLastFrameStartTime = mFrameStartTime;
        mFrameStartTime = Clock::now();
        MilliSeconds elapsedTime = (mFrameStartTime - mLastFrameStartTime);

        if (1.0s < mFrameStartTime - fpsLastUpdateTime) {
            fpsText.setString(" FPS: " + std::to_string(frames));
            fpsLastUpdateTime = Clock::now();
            frames = 0;
        }
        frames++;

        ProcessEvents();
        UpdateCameraPosition(elapsedTime);
        UpdateEntities(elapsedTime);

        mRenderer.SetViewTransform(mCamera.GetViewTransform());
        mRenderer.Render(mFrameBuffer, mSceneEntities);
        mWindowTexture.update(mFrameBuffer.data());

        mWindow.clear();
        mWindow.draw(mWindowSprite);
        mWindow.draw(fpsText);
        mWindow.display();
    }
}


////////////////////////////////////////////////////////////////////////
//
// Scene private methods
//
////////////////////////////////////////////////////////////////////////


void Scene::CreateWindow()
{
    sf::VideoMode mode(mResolution.x, mResolution.y, 32);
    mWindow.create(mode, "Software Renderer", mWindowStyle);
    mWindow.setMouseCursorVisible(false);
    mWindow.setKeyRepeatEnabled(false);
    mWindow.setVerticalSyncEnabled(mWindowVsync);
    CentreMouseInWindow();
}


void Scene::CentreMouseInWindow()
{
    sf::Vector2i centre;
    centre.x = static_cast<int>(mResolution.x)/2;
    centre.y = static_cast<int>(mResolution.y)/2;
    sf::Mouse::setPosition(centre, mWindow);
}


void Scene::SetResolution(Vector2<unsigned int> resolution)
{
    mResolution = resolution;
    mWindowTexture.create(resolution.x, resolution.y);
    mWindowSprite.setTexture(mWindowTexture, true);
    mFrameBuffer.resize(4*resolution.x*resolution.y, 255);
    mRenderer.SetResolution(resolution);

    // Update the vertical fov to match the new resolution.
    SetFOV(mFov.x, true);

    // Recreate window if it is open.
    if (mWindow.isOpen()) {
        CreateWindow();
    }
}


void Scene::SetFOV(Real degrees, bool horizontal)
{
    // Physical monitor set-up, used to compute proper pixel aspect ratios for FOV and zoom values.
    // These should ideally be retrieved from the system.
    const Vector2<unsigned int> monitorAspect{16, 9};
    const Vector2<unsigned int> monitorResolution{1920, 1080};

    // We compute the zoom values along side the fov. The zoom is passed to the renderer.
    Vector2<Real> zoom;

    Real monitorAspectFrac = Real(monitorAspect.x)/Real(monitorAspect.y);
    Real monitorResolutionFracInv = Real(monitorResolution.y)/Real(monitorResolution.x);
    Real windowResolutionFrac = Real(mResolution.x)/Real(mResolution.y);
    if (horizontal) {
        // Compute horizontal zoom/fov and adjust vertical to fit window.
        mFov.x = degrees;
        zoom.x = 1.0/Math::Tan(Math::DegToRad(degrees)/2.0);
        zoom.y = zoom.x*windowResolutionFrac*monitorAspectFrac*monitorResolutionFracInv;
        mFov.y = 2.0*Math::RadToDeg(Math::Atan(1.0/zoom.y));
    } else {
        // Compute vertical zoom and adjust horizontal to fit window.
        mFov.y = degrees;
        zoom.y = 1.0/Math::Tan(Math::DegToRad(degrees)/2.0);
        zoom.x = zoom.y/(windowResolutionFrac*monitorAspectFrac*monitorResolutionFracInv);
        mFov.x = 2.0*Math::RadToDeg(Math::Atan(1.0/zoom.x));
    }

    // Update renderer
    mRenderer.SetZoom(zoom);
}



void Scene::UpdateCameraPosition(Seconds elapsedTime)
{
    if (mKeyIsPressed[sf::Keyboard::W]) {
        mCamera.MoveForward(elapsedTime);
    }
    if (mKeyIsPressed[sf::Keyboard::S]) {
        mCamera.MoveBackwards(elapsedTime);
    }
    if (mKeyIsPressed[sf::Keyboard::D]) {
        mCamera.StrafeRight(elapsedTime);
    }
    if (mKeyIsPressed[sf::Keyboard::A]) {
        mCamera.StrafeLeft(elapsedTime);
    }
    if (mKeyIsPressed[sf::Keyboard::E]) {
        mCamera.TiltRight(elapsedTime);
    }
    if (mKeyIsPressed[sf::Keyboard::Q]) {
        mCamera.TiltLeft(elapsedTime);
    }
}


void Scene::UpdateEntities(Seconds elapsedTime)
{
    // Pass the elapsed time to entities that has instructions.
    for (Entity& entity : mSceneEntities) {
        if (entity.instructionsEnabled) {
            entity.Update(elapsedTime);
        }
    }
}


void Scene::ProcessEvents()
{
    // Retrieve all window events that have happened since last ProcessEvents call.
    sf::Event event;
    while (mWindow.pollEvent(event)) {
        switch (event.type) {
            case sf::Event::LostFocus:
                HandleLostFocus();
                break;
            case sf::Event::KeyPressed:
                HandleKeyPress(event);
                break;
            case sf::Event::KeyReleased:
                HandleKeyRelease(event);
                break;
            case sf::Event::MouseMoved:
                HandleMouseMove(event);
                break;
            case sf::Event::Closed:
                mWindow.close();
                break;
            default:
                break;
        }
    }

    // Reset the mouse position to the window centre.
    CentreMouseInWindow();
}


void Scene::HandleLostFocus()
{
    // Do nothing until focus is regained.
    sf::Event event;
    do {
        mWindow.waitEvent(event);
    } while (event.type != sf::Event::GainedFocus);

    // Reset the keyboard presses.
    mKeyIsPressed.clear();

    // Reset the frame time.
    mFrameStartTime = Clock::now();
    mLastFrameStartTime = mFrameStartTime;
}


void Scene::HandleKeyPress(const sf::Event& event)
{
    // Update the keyboard state.
    mKeyIsPressed[event.key.code] = true;

    // Process any key press triggers.
    switch (event.key.code) {
        case sf::Keyboard::G:
            mRenderer.DrawWireframes(!mRenderer.IsDrawingWireframes());
            break;
        case sf::Keyboard::H:
            mRenderer.DrawColours(!mRenderer.IsDrawingColours());
            break;
        case sf::Keyboard::Y:
            mWindowVsync = !mWindowVsync;
            mWindow.setVerticalSyncEnabled(mWindowVsync);
            break;
        case sf::Keyboard::R:
            if (mWindowStyle != sf::Style::Fullscreen) {
                mWindowStyle = sf::Style::Fullscreen;
                SetResolution(Vector2<unsigned int>{1920, 1080});
                break;
            }
            mWindowStyle = sf::Style::Default;
            SetResolution(Vector2<unsigned int>{800, 800});
            break;
        case sf::Keyboard::T:
            if (Math::IsEqual(mFov.x, 120.0)) {
                SetFOV(90.0, true);
                break;
            }
            SetFOV(120.0, true);
            break;
        case sf::Keyboard::Num1:
            mSceneEntities[0].instructionsEnabled = !mSceneEntities[0].instructionsEnabled;
            break;
        case sf::Keyboard::Num2:
            if (1.0 < mCamera.moveSpeed) {
                mCamera.moveSpeed = 0.1;
                break;
            }
            mCamera.moveSpeed = 5.0;
            break;
        case sf::Keyboard::Escape:
            mWindow.close();
            break;
        default:
            break;
    }

}


void Scene::HandleKeyRelease(const sf::Event& event)
{
    mKeyIsPressed[event.key.code] = false;
}


void Scene::HandleMouseMove(const sf::Event& event)
{
    Real deltaX = Real(event.mouseMove.x - static_cast<int>(mResolution.x)/2)/Real(mResolution.x);
    Real deltaY = Real(event.mouseMove.y - static_cast<int>(mResolution.y)/2)/Real(mResolution.y);
    mCamera.MouseMoved(deltaX, deltaY);
}

