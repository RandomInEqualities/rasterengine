#ifndef OBJECT_HPP_INCLUDED
#define OBJECT_HPP_INCLUDED

#include <string>
#include <array>
#include <vector>
#include "Core/Types.hpp"
#include "Core/Vector2.hpp"
#include "Core/Vector3.hpp"


class Colour
{
public:

    Colour();
    Colour(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha);

    Uint8 Red();
    Uint8 Green();
    Uint8 Blue();
    Uint8 Alpha();

    void Red(Uint8 red);
    void Green(Uint8 green);
    void Blue(Uint8 blue);
    void Alpha(Uint8 alpha);

    // Store the colour as an RGBA array.
    std::array<Uint8, 4> rgba;

};


class Triangle
{
public:
    // The three vertices of the triangle.
    Vector3<Real> p0;
    Vector3<Real> p1;
    Vector3<Real> p2;

    // The id of the triangle texture (is -1 if it has no texture).
    int textureId = -1;

    // The vertex positions on the texture. The x and y values are between 0.0 and 1.0.
    Vector2<Real> p0TexturePos;
    Vector2<Real> p1TexturePos;
    Vector2<Real> p2TexturePos;

    // The uniform colour. This can be used to colour the triangle if it does not have a texture.
    Colour uniformColour;
};


class Texture
{
public:
    unsigned int width = 0;
    unsigned int height = 0;

    // Store the texture as an RGBA image.
    std::vector<Uint8> rgba;
};


// Class for bundling textures together. It operates with integer id's. When you load a texture
// it will return a unique id that identifies this texture. The texture is then retrieved by using
// the id. The id's are all positive.
class TextureContainer
{
public:

    // Load a texture from a file into the array and return its id. Throws runtime_error if it
    // can't load the texture. If the texture has already been loaded it just returns its id.
    int LoadTexture(const std::string& filename);

    // Get the texture corresponding to id. Throws out_of_range if no texture exists.
    const Texture& GetTexture(int id) const;

private:

    std::vector<Texture> textures;
    std::vector<std::string> filenames;

};


class Model
{
public:

    std::string name;
    std::vector<Triangle> geometry;

    // Load the geometry from a wavefront object file. If the triangles needs textures, they will
    // be loaded into the texture container. Throws runtime_error on failure.
    void LoadFromFile(const std::string& filename, TextureContainer& textures);

};


#endif // OBJECT_HPP_INCLUDED
