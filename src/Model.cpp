////////////////////////////////////////////////////////////////////////
//
// Model.cpp
//
////////////////////////////////////////////////////////////////////////

#include <string>
#include <array>
#include <vector>
#include <stdexcept>
#include <random>

#include "Model.hpp"
#include "Core/Types.hpp"
#include "Core/Vector2.hpp"
#include "Core/Vector3.hpp"
#include "FileFormat/ObjModel.hpp"
#include "FileFormat/TgaImage.hpp"


////////////////////////////////////////////////////////////////////////
//
// Colour class methods
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Colour::Colour()
{
}


////////////////////////////////////////////////////////////////////////
Colour::Colour(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)
{
    rgba[0] = red;
    rgba[1] = green;
    rgba[2] = blue;
    rgba[3] = alpha;
}


////////////////////////////////////////////////////////////////////////
Uint8 Colour::Red()
{
    return rgba[0];
}


////////////////////////////////////////////////////////////////////////
Uint8 Colour::Green()
{
    return rgba[1];
}


////////////////////////////////////////////////////////////////////////
Uint8 Colour::Blue()
{
    return rgba[2];
}


////////////////////////////////////////////////////////////////////////
Uint8 Colour::Alpha()
{
    return rgba[3];
}


////////////////////////////////////////////////////////////////////////
void Colour::Red(Uint8 red)
{
    rgba[0] = red;
}


////////////////////////////////////////////////////////////////////////
void Colour::Green(Uint8 green)
{
    rgba[1] = green;
}


////////////////////////////////////////////////////////////////////////
void Colour::Blue(Uint8 blue)
{
    rgba[2] = blue;
}


////////////////////////////////////////////////////////////////////////
void Colour::Alpha(Uint8 alpha)
{
    rgba[3] = alpha;
}


////////////////////////////////////////////////////////////////////////
//
// TextureContainer methods
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
int TextureContainer::LoadTexture(const std::string& filename)
{
    // check if the texture is already loaded.
    for (size_t index = 0; index < filenames.size(); index++) {
        if (filename == filenames[index]) {
            return index;
        }
    }

    // load the texture.
    Texture newTexture;
    newTexture.rgba = LoadTGAImage(filename, newTexture.width, newTexture.height);
    textures.emplace_back(newTexture);
    filenames.emplace_back(filename);
    return textures.size() - 1;
}


////////////////////////////////////////////////////////////////////////
const Texture& TextureContainer::GetTexture(int id) const
{
    return textures.at(id);
}


////////////////////////////////////////////////////////////////////////
//
// Model methods
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
void Model::LoadFromFile(const std::string& filename, TextureContainer& textures)
{
    name.clear();
    geometry.clear();

    // Read the obj file into an Obj::Model. Throws runtime_error on failure.
    Obj::Model obj;
    obj.ReadFromFile(filename);

    // Convert the model into triangles and textures.
    name = obj.name;

    // Random colour generation.
    std::default_random_engine generator;
    std::uniform_int_distribution<Uint8> uniform(0, 255);

    for (const Obj::Face& face : obj.faces) {

        if (face.vertices.size() != 3) {
            throw std::runtime_error("Only support triangle faces.");
        }

        Triangle triangle;
        triangle.p0 = face.vertices[0];
        triangle.p1 = face.vertices[1];
        triangle.p2 = face.vertices[2];

        if (face.texturePositions.size() == 3) {
            triangle.p0TexturePos = face.texturePositions[0];
            triangle.p1TexturePos = face.texturePositions[1];
            triangle.p2TexturePos = face.texturePositions[2];
        }

        if (face.material.empty()) {
            // give the triangle a random colour.
            triangle.uniformColour.Red(uniform(generator));
            triangle.uniformColour.Green(uniform(generator));
            triangle.uniformColour.Blue(uniform(generator));
            triangle.uniformColour.Alpha(255);
        }
        else {
            // give the triangle the diffuse material colour.
            const Obj::Material& material = obj.GetMaterial(face.material);
            triangle.uniformColour.Red(static_cast<Uint8>(255.0*material.diffuse.x));
            triangle.uniformColour.Green(static_cast<Uint8>(255.0*material.diffuse.y));
            triangle.uniformColour.Blue(static_cast<Uint8>(255.0*material.diffuse.z));
            triangle.uniformColour.Alpha(static_cast<Uint8>(255.0*material.transparency));

            // load the material texture (if any).
            if (!material.diffuseFilename.empty()) {
                triangle.textureId = textures.LoadTexture(material.diffuseFilename);
            }
        }
        geometry.emplace_back(triangle);

    }
}
